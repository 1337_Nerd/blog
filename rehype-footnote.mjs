import { visit } from "astro-expressive-code/hast"

export function rehypeFootnote() {
	return function(tree) {
		visit(tree, 'element', (node) => {
			if (/#user-content-fnref/ig.test(node.properties?.href ?? '') && node.tagName === 'a' && node.properties?.className?.includes('data-footnote-backref'))
				node.children.find(child => child.type === 'text').value = '↩︎'
		})
	}
}