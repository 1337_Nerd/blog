import { defineConfig, passthroughImageService } from 'astro/config';
import { FontaineTransform } from 'fontaine'
import { imagetools } from 'vite-imagetools'
import tailwind from "@astrojs/tailwind";
import mdx from "@astrojs/mdx";
import { remarkReadingTime } from './remark-reading-time.mjs';
import { rehypeFootnote } from './rehype-footnote.mjs';
import svelte from "@astrojs/svelte";
import expressiveCode from "astro-expressive-code";
import cloudflare from "@astrojs/cloudflare";
import caddy from './caddy.json'

// https://astro.build/config
export default defineConfig({
  site: 'https://blog.joshuastock.net',
  build: {
    format: 'file',
    inlineStylesheets: 'never'
  },
  vite: {
    build: {
      assetsInlineLimit: 0
    },
    plugins: [
      imagetools({
        cache: {
          enabled: true,
          dir: './node_modules/.astro/.cache/imagetools'
        }
      }),
      FontaineTransform.vite({
        fallbacks: ['-apple-system', 'BlinkMacSystemFont', 'Segoe UI', 'Roboto', 'Helvetica Neue', 'Helvetica', 'Arial'],
        resolvePath: (id) => new URL(`.${id}`, import.meta.url),
      })
    ]
  },
  image: {
    service: passthroughImageService()
  },
  integrations: [
    tailwind({
      applyBaseStyles: false
    }),
    expressiveCode({
      styleOverrides: {
        borderRadius: '1rem',
        frames: {
          frameBoxShadowCssValue: 'none'
        }
      },
      plugins: [{
        name: 'custom-css',
        baseStyles: 'margin: 2rem auto'
      }],
      shiki: {
        langs: [
          {
            id: 'caddyfile',
            scopeName: 'source.Caddyfile',
            aliases: ['caddy'],
            ...caddy
          }
        ]
      }
    }), mdx(), svelte()
  ],
  markdown: {
    remarkPlugins: [remarkReadingTime],
    rehypePlugins: [rehypeFootnote],
    extendDefaultPlugins: true
  },
  prefetch: {
    prefetchAll: true
  },
  output: "server",
  adapter: cloudflare({
    imageService: 'passthrough',
    platformProxy: {
      enabled: true
    }
  }),
  security: {
    checkOrigin: true
  },
  server: {
    host: '0.0.0.0'
  }
});