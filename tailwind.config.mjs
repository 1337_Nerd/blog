import defaultTheme from 'tailwindcss/defaultTheme'
import plugin from 'tailwindcss/plugin'

/** @type {import('tailwindcss').Config} */
export default {
	content: ['./src/**/*.{astro,html,js,jsx,md,mdx,svelte,ts,tsx,vue}'],
	theme: {
		extend: {
			animation: {
				fadeIn: '.5s ease-in forwards fadein',
				openModal: '.2s ease-in forwards openmodal',
				closeModal: '.2s ease-out forwards closemodal',
				moveIn: '.7s ease-out forwards movein'
			},
			colors: {
				highlight: '#b043d1',
				light: '#fdfdfd',
				'light-shadow': '#757575',
				'dark-shadow': '#040407',
				dark: '#1d1f28',
				border: '#d0d1d2',
				'border-dark': '#4a4b53',
				card: '#ededed',
				'card-dark': '#2b2e3c',
				'border-focus': '#8d8e93',
				dropdown: '#7e7e7e',
				'dropdown-dark': '#3a3e50',
				average: '#e143a4'
			},
			fontFamily: {
				sans: ['"Brandon Grotesque"', '"Brandon Grotesque fallback"', ...defaultTheme.fontFamily.sans]
			},
			keyframes: {
				fadein: {
					'0%': { opacity: 0.2 },
					'100%': { opacity: 0.9 }
				},
				openmodal: {
					'0%': { opacity: 0 },
					'100%': { opacity: 1, transform: 'translateY(-1rem)' }
				},
				closemodal: {
					'0%': { opacity: 1, transform: 'translateY(-1rem)' },
					'100%': { opacity: 0, transform: 'translateY(0)' }
				},
				heartbeat: {
					'50%': { scale: '1.2' }
				},
				movein: {
					'0%': {
						opacity: 0,
						transform: 'translateY(-.75rem)'
					},
					'100%': {
						opacity: 1,
						transform: 'translateY(0)'
					}
				}
			},
			screens: {
				'3xl': '1600px',
				'4xl': '2000px',
				'5xl': '2500px',
				'6xl': '3000px'
			},
			typography: {
				DEFAULT: {
					css: {
						'code::before': {
							content: ''
						},
						'code::after': {
							content: ''
						}
					}
				}
			}
		}
	},
	plugins: [
		require('@tailwindcss/typography'),
		plugin(function({ matchUtilities, theme }) {
			matchUtilities(
				{
					'animate-delay': (value) => ({
						animationDelay: value
					}),
				},
				{
					values: theme('transitionDelay')
				}
			)
		})
	],
}
