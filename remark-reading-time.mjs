import getReadingTime from 'reading-time';
import { toString } from 'mdast-util-to-string';

export function remarkReadingTime() {
	return function (tree, { data }) {
		//const filteredTree = { type: 'root', children: tree.children.filter((child) => child.type !== 'yaml')}
		const textOnPage = toString(tree);
		const readingTime = getReadingTime(textOnPage);
		data.astro.frontmatter.minutesRead = readingTime.text;
		data.astro.frontmatter.words = textOnPage.slice(0, 400).trim();
	};
}