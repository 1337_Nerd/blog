import { defineMiddleware } from 'astro:middleware'

async function generateCSPHash(style: string) {
	const buffer = await crypto.subtle.digest('SHA-384', new TextEncoder().encode(style))
	return `'sha384-${btoa(String.fromCharCode(...new Uint8Array(buffer)))}'`
}

export const onRequest = defineMiddleware(async(context, next) => {
	const response = await next()
	const newHeaders = new Headers(response.headers)
	newHeaders.set('Cross-Origin-Resource-Policy', 'same-site')
	newHeaders.set('Referrer-Policy', 'strict-origin-when-cross-origin')
	newHeaders.set('X-Content-Type-Options', 'nosniff')
	if (!newHeaders.get('Content-Type')?.toLowerCase().startsWith('text/html'))
		return new Response(response.body, {
			status: response.status,
			statusText: response.statusText,
			headers: newHeaders
		})
	const start = performance.now()
	const nonce = btoa(String.fromCharCode(...crypto.getRandomValues(new Uint8Array(16))))
	const html = await response.text()
	const styles = new Set(Array.from(html.matchAll(/style="([^"]*)"/gi), match => match[1]));
	const hashes = await Promise.all(Array.from(styles).map(async(style) => await generateCSPHash(style)))
	const noncedHtml = html.replace(/<script(?![^>]*type=['"]application\/ld\+json['"])([^>]*)>/gi, (match, p1) => `<script${p1} nonce='${nonce}'>`).replace(/<style([^>]*)>/gi, (match, p1) => `<style${p1} nonce="${nonce}">`)
	if (!newHeaders.has('Content-Security-Policy'))
		newHeaders.set('Content-Security-Policy', `default-src 'none'; script-src 'self' https://umami.joshuastock.net/script.js 'nonce-${nonce}' 'strict-dynamic' 'unsafe-inline'; style-src 'self' 'nonce-${nonce}' 'strict-dynamic' 'unsafe-inline'; style-src-attr ${hashes.join(' ')} 'unsafe-hashes'; object-src 'none'; base-uri 'self'; connect-src 'self' https://jellyfin.joshuastock.net https://umami.joshuastock.net https://v4.passwordless.dev; font-src 'self'; frame-src 'self'; img-src 'self' https://gravatar.com https://jellyfin.joshuastock.net data:; manifest-src 'self'; media-src 'self'; worker-src 'none'; form-action 'self'; frame-ancestors 'none';`)
	const timings = newHeaders.get('Server-Timing')
	newHeaders.set('Server-Timing', `${timings},nonce;dur=${performance.now() - start};desc="Generate nonce and hashes"`)
	return new Response(noncedHtml, {
		status: response.status,
		statusText: response.statusText,
		headers: newHeaders
	})
})