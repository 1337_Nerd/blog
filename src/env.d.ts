/// <reference path="../.astro/types.d.ts" />
/// <reference types="astro/client" />

type D1Database = import("@cloudflare/workers-types").D1Database
type ENV = {
	ACCESS_TOKEN: string;
	PUBLIC: string;
	API: string;
	ENCRYPTION_SECRET: string;
	IV: string;
	PLAUSIBLE_URL: string;
	PLAUSIBLE_API: string;
	DB: D1Database;
	KEYS: D1Database;
	webhookUrl: string;
}
type Runtime = import("@astrojs/cloudflare").Runtime<ENV>
type TrackedProperties = { hostname: string; language: string; referrer: string; screen: string; title: string; url: string; website: string }
type EventProperties = { name: string; data?: EventData } & TrackedProperties & { website: string }
type PageViewProperties = TrackedProperties & { website: string }
type CustomEventFunction = (props: PageViewProperties) => EventProperties | PageViewProperties
interface EventData { [key: string]: number | string | EventData | number[] | string[] | EventData[] }
interface Window {
	umami?: {
		track: {
			(): Promise<string>,
			(eventName: string): Promise<string>,
			(eventName: string, obj: EventData): Promise<string>,
			(properties: PageViewProperties): Promise<string>,
			(eventFunction: CustomEventFunction): Promise<string>
		}
	}
}
declare namespace App {
	interface Locals extends Runtime {}
	declare module "*&imagetools" {
		/**
		 * actual types
		 * - code https://github.com/JonasKruckenberg/imagetools/blob/main/packages/core/src/output-formats.ts
		 * - docs https://github.com/JonasKruckenberg/imagetools/blob/main/docs/guide/getting-started.md#metadata
		 */
		const out: string | string[]
		export default out
	}
}