export const prerender = true

export async function GET() {
    const text = [{
        "user_agent": "prefetch-proxy",
        "google_prefetch_proxy_eap": {
            "fraction": 1.0
        }
    }]

    return new Response(JSON.stringify(text, null, 2), {
        headers: {
            'Content-Type': 'application/trafficadvice+json'
        }
    })

}