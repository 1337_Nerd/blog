export const prerender = true
import type { APIContext } from 'astro'

const addDays = function(date: Date, days: number): Date {
	const newDate = new Date(date)
	newDate.setDate(newDate.getDate() + days)
	return newDate
}

export async function GET({ site }: APIContext) {
	const nextYear: Date = addDays(new Date(), 365)
	
	const text: string = `
	Contact: https://joshuastock.net
	Contact: mailto:security@${site!.hostname}
	Expires: ${nextYear.toISOString()}
	Preferred-Languages: en
	Canonical: ${site!.toString()}.well-known/security.txt
	`.replace(/\t/g, '').trim()
	
	return new Response(text, {
        headers: {
		    'Content-Type': 'text/plain; charset=utf-8',
        }
    })
}