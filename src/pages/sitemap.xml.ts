import { getCollection } from 'astro:content'
import about from './about.mdx'
import privacy from './privacy.mdx'
import type { APIContext } from 'astro'
export const prerender = true

export async function GET({ site }: APIContext) {
	const posts = (await getCollection('posts')).sort((a, b) => +new Date(b.data.pubDate) - +new Date(a.data.pubDate))
	const mostRecent = posts[0]?.data.pubDate
	const newestPage = [+new Date(mostRecent), +new Date(about({}).props.pubDate), +new Date(privacy({}).props.pubDate)].sort().reverse()[0]
	const pages = [
		{ slug: 'sitemap-authors.xml', lastmod: mostRecent },
		{ slug: 'sitemap-pages.xml', lastmod: newestPage },
		{ slug: 'sitemap-posts.xml', lastmod: mostRecent },
		{ slug: 'sitemap-tags.xml', lastmod: mostRecent }
	]
	const xmlString = `<?xml version="1.0" encoding="UTF-8" ?>
	<sitemapindex
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
	xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
	${pages.map(page => `<sitemap><loc>${site!.toString()}${page.slug}</loc><lastmod>${new Date(page.lastmod).toISOString()}</lastmod></sitemap>`).join('')}
	</sitemapindex>`.trim()
	return new Response(xmlString, {
		headers: {
			'Cache-Content': 'public, max-age=0, must-revalidate',
			'X-Robots-Tag': 'noindex, follow',
			'Content-Type': 'application/xml',
		},
	})
}
