import type { APIContext } from "astro"

export async function GET({ request }: APIContext) {
	const uuid = new URL(request.url).searchParams.get('uuid')
	return new Response(null, {
		headers: {
			'Location': `/?uuid=${uuid}&action=unsubscribe`
		},
		status: 302
	})
}

export async function POST({ locals: { runtime: { env: { DB } } }, request }: APIContext) {
	const uuid = new URL(request.url).searchParams.get('uuid')
	if (!uuid || !/[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}/i.test(uuid))
		return new Response(null, { status: 404 })
	const { success } = await DB.prepare('UPDATE users SET subscribed = ? WHERE id = ?').bind(false, uuid).run()
	return new Response(null, { status: 201 })
}