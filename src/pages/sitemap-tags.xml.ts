import { getCollection } from 'astro:content'
import type { APIContext } from 'astro'
export const prerender = true

export async function GET({ site }: APIContext) {
	const posts = (await getCollection('posts')).sort((a, b) => +new Date(b.data.pubDate) - +new Date(a.data.pubDate))
	const tags = Array.from(new Set(posts.flatMap(post => post.data.tags)))
	const xmlString = `<?xml version="1.0" encoding="UTF-8" ?>
	<urlset
	xmlns="http://www.sitemaps.org/schemas/sitemap/0.9"
	xmlns:news="http://www.google.com/schemas/sitemap-news/0.9"
	xmlns:xhtml="http://www.w3.org/1999/xhtml"
	xmlns:image="http://www.google.com/schemas/sitemap-image/1.1"
	xmlns:video="http://www.google.com/schemas/sitemap-video/1.1">
	${tags.filter(tag => !!tag).map(tag => `<url><loc>${site!.toString()}tags/${tag}</loc><lastmod>${new Date(posts.find(post => post.data.tags?.includes(tag ?? ''))?.data.pubDate ?? +new Date()).toISOString()}</lastmod></url>`).join('')}
	</urlset>`.trim()
	return new Response(xmlString, {
		headers: {
			'Cache-Content': 'public, max-age=0, must-revalidate',
			'X-Robots-Tag': 'noindex, follow',
			'Content-Type': 'application/xml',
		},
	})
}
