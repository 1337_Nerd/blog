import type { APIContext } from "astro"

export async function POST({ locals: { runtime: { env: { PUBLIC } } }, request }: APIContext) {
	const data = await request.json()
	const response = await fetch('https://v4.passwordless.dev/signin/begin', {
		method: 'POST',
		headers: {
			'ApiKey': PUBLIC,
			'Content-Type': 'application/json',
			'Client-Version': 'js-1.1.0'
		},
		body: JSON.stringify({
			userId: undefined,
			alias: data.alias,
			RPID: data.rpid,
			Origin: data.Origin
		})
	})
	const res = await response.json()
	if (response.ok)
		return new Response(JSON.stringify(res))
	return new Response(JSON.stringify({...res, 'from': 'Server'}), {status: response.status})
}