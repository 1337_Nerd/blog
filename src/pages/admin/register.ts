import getSHA384Hash from "@lib/hash";
import type { APIContext } from "astro";

export async function POST({ locals: { runtime: { env: { DB, API } } }, request }: APIContext) {
	const { username, code } = await request.json()
	const result = await DB.prepare('SELECT exp FROM invitations WHERE token = ?').bind(await getSHA384Hash(code)).first<{exp: number}>()
	if (!result?.exp || result?.exp < Date.now())
		return new Response(JSON.stringify({'error': 'Invalid code'}), {status: 400})
	const aliases: string[] = new Array(1)
	aliases[0] = username.trim().toLowerCase()
	const payload = {
		userId: crypto.randomUUID(),
		username: username.trim().toLowerCase(),
		attestation: 'none',
		discoverable: true,
		userVerification: 'preferred',
		aliases: aliases,
		aliasHashing: false
	}
	const response = await fetch('https://v4.passwordless.dev/register/token', {
		method: 'POST',
		body: JSON.stringify(payload),
		headers: {
			'ApiSecret': API,
			'Content-Type': 'application/json',
		}
	})
	const res = await response.json()
	if (response.ok)
		return new Response(JSON.stringify(res))
	return new Response(JSON.stringify({...res, 'from': 'Server'}), {status: response.status})
}
