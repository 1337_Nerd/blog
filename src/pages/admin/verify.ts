import getSHA384Hash from "@lib/hash";
import decrypt from "@lib/decrypt";
import Sign from "@lib/hmac";
import type { APIContext } from "astro";

export async function GET({ locals: { runtime: { env: { DB, API, KEYS, ENCRYPTION_SECRET, IV } } }, request, site }: APIContext) {
	try {
		const params = new URL(request.url).searchParams
		const token = params.get('token')
		const response = await fetch(`https://v4.passwordless.dev/signin/verify?token=${token}`, {
			headers: {
				'ApiSecret': API,
				'Content-Type': 'application/json'
			},
			method: 'POST',
			body: JSON.stringify({ token })
		})
		const { success, userId, timestamp, tokenId } = await response.json()
		if (!success)
			throw new Error('Verification failed')
		const iat = new Date(timestamp).getTime() / 1000
		const exp = new Date((iat * 1000) + 14400000).getTime() / 1000
		const aud = userId
		const sub = tokenId
		const iss = site!.origin
		const hashedAuth = await getSHA384Hash(`${userId}${tokenId}`)
		const authorizeToken = await DB.prepare('INSERT INTO tokens (access_token, user_id, iat, exp) VALUES (?, ?, ?, ?) ON CONFLICT DO NOTHING').bind(hashedAuth, userId, iat, exp).run()
		if (!authorizeToken.success)
			return new Response('Error signing in', {status: 500})
		const now = Date.now() / 1000
		const currentKey = await KEYS.prepare('SELECT secret, alg, algorithmName, kid FROM keys WHERE iat < ? AND exp > ?').bind(now, now).first<{secret: string, alg: string, algorithmName: string, kid: string}>()
		if (!currentKey)
			throw Error('No valid keys found')
		const { secret, alg, algorithmName, kid } = currentKey
		const decryptedSecret = await decrypt(secret, ENCRYPTION_SECRET, IV)
		const JWT = await Sign({ sub, iat, exp, aud, iss }, decryptedSecret, alg, algorithmName, kid)
		const res = new Response('Success', {
			headers: {
				'Set-Cookie': `admin-api-session=${JWT}; Path=/admin; Expires=${new Date(exp * 1000).toUTCString()}; SameSite=Lax; Secure; HostOnly=true; HttpOnly=true`,
				'Location': '/admin/dashboard'
			},
			status: 302
		})
		return res
	}
	catch (error) {
		if (!(error instanceof Error))
			return new Response(JSON.stringify({'error': 'Unspecified error'}), {status: 500})
		console.error(error.message)
		return new Response(JSON.stringify({'error': error.message}), {status: 500})
	}
}