import type { APIContext } from "astro";

export async function GET({ locals: { runtime: { cf } } }: APIContext) {
	return new Response(JSON.stringify(cf), {
		headers: {
			'Content-Type': 'application/json'
		},
		status: 200
	})
}
