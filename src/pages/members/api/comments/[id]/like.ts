import Verify from "@lib/verifyMember";
import type { APIContext } from "astro";
export async function POST({ locals: { runtime: { env: { DB } } }, request, params }: APIContext) {
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return new Response(JSON.stringify({'error': 'Unauthorized'}), { headers: { 'Content-Type': 'application/json' }, status: 401 })
	const { id } = verify
	const now = new Date().getTime()
	const uuid = crypto.randomUUID()
	const { success } = await DB.prepare('INSERT INTO comment_likes (id, comment_id, member_id, created_at, updated_at) VALUES (?1, ?2, ?3, ?4, ?4) ON CONFLICT (comment_id, member_id) DO NOTHING').bind(uuid, params.id, id, now).run()
    return new Response(null, { status: 204 })
}
export async function DELETE({ locals: { runtime: { env: { DB } } }, request, params }: APIContext) {
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return new Response(JSON.stringify({'error': 'Unauthorized'}), { headers: { 'Content-Type': 'application/json' }, status: 401 })
	const { id } = verify
	const { success } = await DB.prepare('DELETE FROM comment_likes WHERE comment_id = ? AND member_id = ?').bind(params.id, id).run()
	return new Response(null, { status: 204 })
}