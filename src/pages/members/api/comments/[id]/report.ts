import Verify from "@lib/verifyMember";
import type { APIContext } from "astro";
export async function POST({ locals: { runtime: { env: { DB, webhookUrl } } }, request, params }: APIContext) {
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return new Response(JSON.stringify({'error': 'Unauthorized'}), { headers: { 'Content-Type': 'application/json' }, status: 401 })
	const commentId = params.id
    const comment = await DB.prepare('SELECT u.name, c.html, u.email FROM comments c INNER JOIN Users u ON c.member_id = u.id WHERE c.id = ?').bind(commentId).first<{name: string, html: string, email: string}>()
    if (!comment)
        return new Response(JSON.stringify({'error': 'Comment not found'}), { headers: { 'Content-Type': 'application/json' }, status: 404 })
    const { name, email, html } = comment
    const webhookBody = {
        embeds: [{
            title: 'New comment reported',
            fields: [
                { name: 'From', value: verify.name },
                { name: 'Name', value: name },
                { name: 'Email', value: email },
                { name: 'Post', value: request.headers.get('referer') },
                { name: 'Content', value: html }
            ]
        }]
    }
    const res = await fetch(webhookUrl, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(webhookBody)
    })
    if (!res.ok)
		return new Response(JSON.stringify({'error': 'Error sending report'}), { status: 500, headers: { 'Content-Type': 'application/json' } })
    return new Response(null, { status: 204 })
}