import getSHA384Hash from "@lib/hash";
import Verify from "@lib/verifyMember";
import type { D1Result } from '@cloudflare/workers-types'
import type { APIContext } from "astro";

export async function PUT({ locals: { runtime: { env: { DB } } }, request, params }: APIContext) {
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return new Response(JSON.stringify({'error': 'Unauthorized'}), { headers: { 'Content-Type': 'application/json' }, status: 401 })

	const { id } = verify
	const commentId = params.id
	const { status, comment }: { status?: string, comment?: string } = Object.fromEntries((await request.formData()).entries())
	const now = new Date().getTime()
	let ret
	if (status === 'deleted') {
		const { success }: { success: boolean } = await DB.prepare('DELETE FROM comments WHERE id = ? AND member_id = ?').bind(commentId, id).run()
		if (success)
			ret = {}
	}
	else {
		const rows: D1Result<{ id: string; member_id: string; name: string; email: string; parent_id: string | null; status: "published" | "hidden" | "deleted"; html: string; edited_at: number; created_at: number; update_at: number; like_count: number; liked_by_user: 0 | 1; }>[] = await DB.batch([
			DB.prepare('UPDATE comments SET html = ?1, edited_at = ?2, update_at = ?2 WHERE id = ?3 AND member_id = ?4').bind(comment, now, commentId, id),
			DB.prepare('SELECT c.id, c.member_id, u.name, u.email, c.parent_id, c.status, c.html, c.edited_at, c.created_at, c.update_at, COUNT(DISTINCT cl.id) AS like_count, MAX(CASE WHEN cl.member_id = ?1 THEN 1 ELSE 0 END) AS liked_by_user FROM comments c LEFT JOIN users u ON c.member_id = u.id LEFT JOIN comment_likes cl ON c.id = cl.comment_id WHERE c.id = ?2 AND c.member_id = ?1 GROUP BY c.id').bind(id, commentId)
		])
		ret = (await formatComments(rows[1].results || []))[0]
	}
	return new Response(JSON.stringify(ret), {
		status: 200,
		headers: {
			'Content-Type': 'application/json'
		}
	})
}

async function digestMessage(message: string) {
	const msgUint8 = new TextEncoder().encode(message)
	const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8)
	const hashArray = Array.from(new Uint8Array(hashBuffer))
	const hashHex = hashArray
		.map((b) => b.toString(16).padStart(2, '0'))
		.join('')
	return hashHex
}
async function formatComments(results: { id: string; member_id: string; avatar_image?: string; replies?: []; name: string; email: string; parent_id: string | null; status: "published" | "hidden" | "deleted"; html: string; edited_at: number; created_at: number; update_at: number; like_count: number; liked_by_user: 0 | 1; }[]) {
	const comments = []
	const replyMap = new Map()
	results.sort((_, b) => +(b.parent_id === null) || -1)
	for await (const comment of results) {
		comment.avatar_image = `https://gravatar.com/avatar/${await digestMessage(comment.email)}?d=blank&s=`
		comment.member_id = await getSHA384Hash(comment.member_id)
		delete (comment as {email?: string}).email
		if (comment.parent_id === null) {
			comment.replies = []
			comments.push(comment)
			replyMap.set(comment.id, comment)
		}
		else
			replyMap.get(comment.parent_id)?.replies?.push(comment) || comments.push(comment)
	}
	return comments
}