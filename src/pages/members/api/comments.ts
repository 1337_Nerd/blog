import Verify from "@lib/verifyMember";
import getSHA384Hash from '@lib/hash'
import type { D1Result } from '@cloudflare/workers-types'
import type { APIContext } from "astro";

export async function POST({ locals: { runtime: { env: { DB } } }, request }: APIContext) {
	const { comment, parent_id } = Object.fromEntries((await request.formData()).entries())
	if (!comment)
		return new Response(null, { status: 204 })
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return new Response(JSON.stringify({'error': 'Unauthorized'}), { headers: { 'Content-Type': 'application/json' }, status: 401 })
	const { name, email, id } = verify
	const postId = new URL(request.headers.get('referer') ?? '').pathname.substring(1)
	const hashedId = await getSHA384Hash(postId)
	const now = new Date().getTime()
	const uuid = crypto.randomUUID()
	const rows: D1Result<{id: string, post_id: string, member_id: string, parent_id: string, html: string, created_at: Date, update_at: Date, name?: string, avatar_image?: string, liked_by_user?: 0 | 1, like_count?: number, replies?: any[]}>[] = await DB.batch([
		DB.prepare('INSERT INTO comments (id, post_id, member_id, parent_id, html, created_at, update_at) VALUES (?, ?, ?, ?, ?, ?, ?)').bind(uuid, hashedId, id, parent_id || null, comment, now, now),
		DB.prepare('SELECT * FROM comments WHERE id = ?').bind(uuid)
	])
	const { results } = rows[1]
	for await (const result of results) {
		result.name = name
		result.avatar_image = `https://gravatar.com/avatar/${await digestMessage(email)}?d=blank&s=`
		result.liked_by_user = result.like_count = 0
		result.member_id = await getSHA384Hash(result.member_id)
		if (!parent_id)
			result.replies = []
	}
	return new Response(JSON.stringify(results), {
		headers: {
			'Content-Type': 'application/json',
			'Location': request.headers.get('referer') ?? '/'
		},
		status: 303
	})
}

async function digestMessage(message: string) {
	const msgUint8 = new TextEncoder().encode(message)
	const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8)
	const hashArray = Array.from(new Uint8Array(hashBuffer))
	const hashHex = hashArray
		.map((b) => b.toString(16).padStart(2, '0'))
		.join('')
	return hashHex
}