import sendLink from '@lib/email'
import type { APIContext } from 'astro'

export async function POST({ locals: { runtime: { env: { DB, API } } }, request, site }: APIContext) {
	const referrer = request.headers.get('referer') ?? site!.origin
	try {
		const formData = await request.formData()
		const { $name, email } = Object.fromEntries(Array.from(formData.entries()).map(([key, value]) => [key, key === 'email' ? value.toString().trim().toLowerCase() : value.toString().trim()]))
		if (!email || !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(email.trim()))
			return new Response(JSON.stringify({'error': 'Please enter a valid email'}), {status: 400})
		const id = await DB.prepare('SELECT id FROM users WHERE email = ?').bind(email).first<string>('id')
		if (id)
			return sendLink(email, id, referrer, API, site!.toString())
		if (!$name)
			return new Response(JSON.stringify({'error': 'No member exists with this email address. Please sign up first'}), {status: 400})
		const UUID = crypto.randomUUID()
		const { success } = await DB.prepare('INSERT INTO users (id, name, email, subscribed, verified, trackVisits, emailUUID, newEmail) VALUES (?, ?, ?, ?, ?, ?, ?, ?)').bind(UUID, $name, email, true, false, true, null, null).run()
		if (!success)
			return new Response(JSON.stringify({'error': 'Error'}), { status: 500, headers: { 'Content-Type': 'application/json' } })
		return sendLink(email, UUID, referrer, API, site!.toString())
	}
	catch (error) {
		if (error instanceof Error) {
			console.error(error.message)
			return new Response(JSON.stringify({'error': error.message}), { status: 500, headers: { 'Content-Type': 'application/json' } })
		}
	}
}