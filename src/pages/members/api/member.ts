import Verify from "@lib/verifyMember";
import sendLink from "@lib/email";
import getSHA384Hash from "@lib/hash";
import { timings } from '@lib/verifyMember'
import type { APIContext } from "astro";
import type { D1Database } from '@cloudflare/workers-types'

export async function GET({ locals: { runtime: { env: { DB } } }, request }: APIContext) {
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return verify
	const payload = {
		"$uuid": await getSHA384Hash(verify.id),
		"email": verify.email,
		"$name": verify.name,
		"subscribed": !!verify.subscribed,
		"trackVisits": !!verify.trackVisits
	}
	return new Response(JSON.stringify(payload), { status: 200, headers: { 'Content-Type': 'application/json', 'Server-Timing': timings.join(',') }})
}

export async function PUT({ locals: { runtime: { env: { DB, API } } }, request, site }: APIContext) {
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return verify
	const { $name, email, subscribed, trackVisits } = await request.json()
	if (!email || !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(email))
		return new Response(JSON.stringify({'error': 'Please enter a valid email'}), { status: 400, headers: { 'Content-Type': 'application/json' } })
	if (verify.email !== email)
		return updateEmail($name, email, verify.id, DB, API, site!.toString(), request.headers.get('Referer') ?? site!.origin)
	const { success } = await DB.prepare('UPDATE users SET name = ?, subscribed = ?, trackVisits = ? WHERE id = ?').bind($name, subscribed, trackVisits, verify.id).run()
	if (!success)
		return new Response(JSON.stringify({'error': 'Error updating profile'}), { status: 500, headers: { 'Content-Type': 'application/json' } })
	return new Response('Success updating profile', {status: 200})
}

async function updateEmail(name: string, newEmail: string, aud: string, db: D1Database, api: string, url: string, referrer: string) {
	const UUID = crypto.randomUUID()
	const { success } = await db.prepare('UPDATE users SET name = ?, newEmail = ?, emailUUID = ? WHERE id = ?').bind(name, newEmail, UUID, aud).run()
	if (!success)
		return new Response(JSON.stringify({'error': 'Error updating email'}), { status: 500, headers: { 'Content-Type': 'application/json' } })
	return sendLink(newEmail, aud, referrer, api, url, {'uuid': UUID})
}