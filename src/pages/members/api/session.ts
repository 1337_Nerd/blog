
import getSHA384Hash from "@lib/hash";
import Verify from '@lib/verifyMember'
import type { APIContext } from "astro";

export async function DELETE({ locals: { runtime: { env: { DB } } }, request }: APIContext) {
	const cookies = request.headers.get('Cookie')
	const verify = await Verify(cookies, DB)
	if (verify instanceof Response)
		return verify
	const sessionId = cookies?.split('; ').find(cookie => cookie.startsWith('sessionId'))?.split('=')[1]
	if (!sessionId)
		return new Response(null, {status: 204})
	const hashedSession = await getSHA384Hash(sessionId)
	const { all }: { all: boolean | undefined } = request.headers.get('Content-Type') === 'application/json' ? await request.json() : {}
    const { success } = all ? await DB.prepare('DELETE FROM tokens WHERE user_id = ?').bind(verify.id).run() : await DB.prepare('DELETE FROM tokens WHERE access_token = ?').bind(hashedSession).run()
	if (!success)
		return new Response('Error signing out', {
			headers: {
				'Set-Cookie': 'sessionId=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT; samesite=lax; secure; httponly'
			},
			status: 500
		})
	return new Response(null, {
		headers: {
			'Set-Cookie': 'sessionId=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT; samesite=lax; secure; httponly'
		},
		status: 204
	})
}