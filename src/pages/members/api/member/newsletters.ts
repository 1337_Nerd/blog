import type { APIContext } from "astro";

export async function GET({ locals: { runtime: { env: { DB } } }, request }: APIContext) {
	const uuid = new URL(request.url).searchParams.get('uuid')
	if (!uuid || !/[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}/i.test(uuid))
		return new Response(null, { status: 404 })
	const res = await DB.prepare('SELECT subscribed FROM users WHERE id = ?').bind(uuid).first<{subscribed: 0 | 1}>()
	if (!res)
		return new Response(null, { status: 404 })
	return new Response(JSON.stringify({
			subscribed: !!res.subscribed
		}), {
			headers: {
				'Content-Type': 'application/json'
			}
		}
	)
}

export async function PUT({ locals: { runtime: { env: { DB } } }, request }: APIContext) {
	const uuid = new URL(request.url).searchParams.get('uuid')
	if (!uuid || !/[a-f0-9]{8}-?[a-f0-9]{4}-?4[a-f0-9]{3}-?[89ab][a-f0-9]{3}-?[a-f0-9]{12}/i.test(uuid))
		return new Response(null, { status: 404 })
	const { subscribed } = await request.json()
	const { success } = await DB.prepare('UPDATE users SET subscribed = ? WHERE id = ?').bind(subscribed, uuid).run()
	if (!success)
		return new Response(JSON.stringify({'error': 'Error updating profile'}), { status: 500, headers: { 'Content-Type': 'application/json' } })
	return new Response('Success updating profile', {status: 200})
}