export const prerender = true
import rss from '@astrojs/rss'
import stylesheet from '@assets/pretty-feed-v3.xsl?url'
import favicon from '@assets/icon.svg?url'
import { getCollection } from 'astro:content'
import MarkdownIt from 'markdown-it'
import type { APIContext } from 'astro'

const parser = new MarkdownIt

async function getImage(image: string) {
	if (image.endsWith('.svg')) {
		const images = import.meta.glob('@assets/hero/*.svg', {
			import: 'default'
		})
		const temp = images[image] as () => Promise<{ src: string, width: number, height: number, format: 'svg' }>
		return await temp()
	}
	const pictures = import.meta.glob(['@assets/hero/*', '!@assets/hero/*.svg'], {
		import: 'default',
		query: {
			w: '1200',
			format: `jpg`,
			as: 'metadata',
			allowUpscale: true,
			imagetools: true
		}
	})
	const temp = pictures[image] as () => Promise<{ src: string; width: number; height: number; format: string; }>
	return await temp()
}

export async function GET({ site }: APIContext) {
	const items = (await getCollection('posts')).sort((a, b) => +new Date(b.data.pubDate) - +new Date(a.data.pubDate))

	const rssItems = await Promise.all(items.map(async ({ data: { pubDate, description, featuredImage, title, author, tags }, slug, body }) => {
		const { src } = featuredImage ? (await getImage(featuredImage)) : { src: '' }
		return {
			title: title,
			pubDate: new Date(pubDate),
			description: description,
			link: `${site}${slug}`,
			customData: `<content:encoded><![CDATA[${parser.render(body)}]]></content:encoded><dc:Creator>${author}</dc:Creator>${tags?.map(tag => `<category>${tag}</category>`).join('')}`.concat(featuredImage ? `<image><url>${site}${src}</url><title>${title}</title><link>${site}${slug}</link></image><media:content url="${site}${src}" medium="image"/>` : '')
		}
	}))

	return rss({
		xmlns: {
			'dc': 'http://purl.org/dc/elements/1.1/',
			'content': 'http://purl.org/rss/1.0/modules/content/',
			'atom': 'http://www.w3.org/2005/Atom',
			'media': 'http://search.yahoo.com/'
		},
		title: 'Updates',
		description: 'Thoughts, stories, and ideas',
		trailingSlash: false,
		stylesheet,
		site: site!.toString(),
		items: rssItems,
		customData: `<language>en</language><atom:link href="${site}/rss.xml" rel="self" type="application/rss+xml"/><image><url>${site}${favicon}</url><title>Updates</title><link>${site}</link></image><ttl>60</ttl>`
	})
}