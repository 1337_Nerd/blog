import getSHA384Hash from '@lib/hash';
import type { APIContext } from 'astro';

export async function GET({ locals: { runtime: { env: { DB, API } } }, request }: APIContext) {
	const timings: string[] = []
	try {
		const params = new URL(request.url).searchParams
		const token = params.get('token')
		const dest = decodeURIComponent(params.get('r') ?? '/')
		const start = performance.now()
		const response = await fetch(`https://v4.passwordless.dev/signin/verify?token=${token}`, {
			headers: {
				'ApiSecret': API,
				'Content-Type': 'application/json'
			},
			method: 'POST',
			body: JSON.stringify({ token })
		})
		timings.push(`verifyToken;dur=${performance.now() - start};desc="passwordless verification"`)
		const { success, userId, timestamp } = await response.json()
		if (!success)
			return new Response(null, {
				headers: {
					'Location': `${dest}?action=validation&success=false`
				},
				status: 302
			})
		const queryStart = performance.now()
		const user = await DB.prepare('SELECT emailUUID, newEmail, name, verified FROM users WHERE id = ?').bind(userId).first()
		timings.push(`queryUser;dur=${performance.now() - queryStart};desc="query user"`)
		if (!user)
			return new Response('User not found', {status: 404})
		const { verified, emailUUID, newEmail } = user
		const issuedAt = new Date(timestamp)
		const iat = issuedAt.getTime() / 1000
		issuedAt.setMonth(issuedAt.getMonth() + 6)
		const exp = issuedAt.getTime() / 1000
		const aud = userId
		let action = 'signin'
		if (params.get('uuid') ?? '' === emailUUID) {
			const { success } = await DB.prepare('UPDATE users SET email = ?, emailUUID = ?, newEmail = ? WHERE id = ?').bind(newEmail, null, null, aud).run()
			action = 'updateEmail'
			if (!success)
				return new Response(null, {
					headers: {
						'Location': `${dest}?action=updateEmail&success=false`
					},
					status: 302
				})
		}
		if (!verified) {
			const { success } = await DB.prepare('UPDATE users SET verified = ?, createdAt = ? WHERE id = ?').bind(true, new Date(timestamp).toISOString(), aud).run()
			action = 'signup'
			if (!success)
				return new Response(null, {
					headers: {
						'Location': `${dest}?action=verifyEmail&success=false`
					},
					status: 302
				})
		}
		const sessionId = btoa(String.fromCharCode(...crypto.getRandomValues(new Uint8Array(21))))
		const hashedAuth = await getSHA384Hash(sessionId)
		const tokenStart = performance.now()
		const authorizeToken = await DB.prepare('INSERT INTO tokens (access_token, user_id, iat, exp) VALUES (?, ?, ?, ?) ON CONFLICT DO NOTHING').bind(hashedAuth, userId, iat, exp).run()
		timings.push(`createToken;dur=${performance.now() - tokenStart};desc="create session token"`)
		if (!authorizeToken.success)
			return new Response(null, {
				headers: {
					'Location': `${dest}?action=validation&success=false`
				},
				status: 302
			})
		const res = new Response('Success', {
			headers: {
				'Set-Cookie': `sessionId=${sessionId}; Path=/; Expires=${new Date(exp * 1000).toUTCString()}; SameSite=lax; Secure; HttpOnly=true`,
				'Location': `${dest}?action=${action}&success=true`,
				'Server-Timing': timings.join(',')
			},
			status: 302
		})
		return res
	}
	catch (error) {
		if (!(error instanceof Error))
			return new Response(JSON.stringify({'error': 'Unspecified error'}), {status: 500, headers: { 'Server-Timing': timings.join(',') }})
		console.error(error)
		return new Response(JSON.stringify({'error': error.message}), {status: 500, headers: { 'Server-Timing': timings.join(',') }})
	}
}