---
title: 'Migrating from API Endpoints to Server-Side Rendering (SSR) in Astro: Lessons Learned'
pubDate: '2024-07-24T00:21:51.227Z'
description: 'Learn how to migrate from API endpoints to Server-Side Rendering (SSR) in Astro, including tips and challenges faced when optimizing images for Cloudflare Pages.'
author: 'Josh Stock'
featuredImage: '/src/assets/hero/mike-van-den-bos-jf1EomjlQi0-unsplash.jpg'
tags: ['meta', 'security', 'astro', 'SSR', 'cloudflare']
---

# Introduction

In this post, I'll walk you through my experience migrating my website from using API endpoints to retrieve member information and comments to using server-side rendering (SSR) with Astro. We'll dive into the code, explore how SSR works, and discuss the challenges I faced when attempting to optimize images for a Cloudflare Pages site.

# The Old Way: Using API Endpoints for Member Information and Comments

Before we get started, let's take a look at the old way of doing things. In my previous setup, I was using API endpoints to fetch member information and comments. This involved making separate requests to the API server and then rendering the data on the client side. Essentially, when a post was loaded, it would make two calls: `/members/api/member` and `/members/api/comments/[id]`. These would then call two pieces of code:

```ts
// /members/api/member
import Verify from "../../../lib/verifyMember";
import sendLink from "../../../lib/email";
import getSHA384Hash from "../../../lib/hash";

export async function GET(context: { locals: { runtime: { env: { DB: any } } }; request: Request }) {
	const { request, locals : { runtime: { env } } } = context
	const { DB } = env
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return verify
	const payload = {
		"$uuid": await getSHA384Hash(verify.id),
		"email": verify.email,
		"$name": verify.name,
		"subscribed": !!verify.subscribed,
		"trackVisits": !!verify.trackVisits
	}
	return new Response(JSON.stringify(payload), {status: 200})
}

export async function PUT(context: { locals: { runtime: { env: { DB: any, API: string } } }; request: Request }) {
	const { request, locals : { runtime: { env } } } = context
	const { API, DB } = env
	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return verify
	const { $name, email, subscribed, trackVisits } = await request.json()
	if (!email || !/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/.test(email))
		return new Response(JSON.stringify({'error': 'Please enter a valid email'}), {status: 400})
	if (verify.email !== email)
		return updateEmail($name, email, verify.id, DB, API, request.headers.get('Referer') ?? 'https://blog.joshuastock.net')
	const { success } = await DB.prepare('UPDATE users SET name = ?, subscribed = ?, trackVisits = ? WHERE id = ?').bind($name, subscribed, trackVisits, verify.id).run()
	if (!success)
		return new Response(JSON.stringify({'error': 'Error updating profile'}), {status: 500})
	return new Response('Success updating profile', {status: 200})
}

async function updateEmail(name: string, newEmail: string, aud: string, db: any, api: string, referrer: string) {
	const UUID = crypto.randomUUID()
	const { success } = await db.prepare('UPDATE users SET name = ?, newEmail = ?, emailUUID = ? WHERE id = ?').bind(name, newEmail, UUID, aud).run()
	if (!success)
		return new Response(JSON.stringify({'error': 'Error updating email'}), {status: 500})
	return sendLink(newEmail, aud, referrer, api, {'uuid': UUID})
}
```

```ts
// /members/api/comments/[id]
import getSHA384Hash from "../../../../lib/hash";
import Verify from "../../../../lib/verifyMember";

export async function GET(context: { locals: { runtime: { env: { DB: any } } }; request: Request; params: { id: string } }) {
	const { request, params, locals : { runtime: { env } } } = context
	const { DB } = env

	const verify = await Verify(request.headers.get('Cookie'), DB)
	const postId = params.id

	const stmt = verify instanceof Response ?
				await DB.prepare('SELECT c.id, c.member_id, u.name, u.email, c.parent_id, c.status, c.html, c.edited_at, c.created_at, c.update_at, COUNT(DISTINCT cl.id) AS like_count FROM comments c LEFT JOIN users u ON c.member_id = u.id LEFT JOIN comment_likes cl ON c.id = cl.comment_id WHERE c.post_id = ? GROUP BY c.id').bind(postId) :
				await DB.prepare('SELECT c.id, c.member_id, u.name, u.email, c.parent_id, c.status, c.html, c.edited_at, c.created_at, c.update_at, COUNT(DISTINCT cl.id) AS like_count, MAX(CASE WHEN cl.member_id = ?1 THEN 1 ELSE 0 END) AS liked_by_user FROM comments c LEFT JOIN users u ON c.member_id = u.id LEFT JOIN comment_likes cl ON c.id = cl.comment_id WHERE c.post_id = ?2 GROUP BY c.id').bind(verify.id, postId)
	const { results }: { results?: {id: string, member_id: string, name: string, email: string, parent_id: string | null, status: 'published' | 'hidden' | 'deleted', html: string, edited_at: number, created_at: number, update_at: number, like_count: number, liked_by_user: 0 | 1}[]} = await stmt.all()
	const comments = await formatComments(results || [])
	return new Response(JSON.stringify(comments), {
		status: 200,
		headers: {
			'Content-Type': 'application/json'
		}
	})
}

export async function PUT(context: { locals: { runtime: { env: { DB: any } } }; request: Request; params: { id: string } }) {
	const { request, params, locals : { runtime: { env } } } = context
	const { DB } = env

	const verify = await Verify(request.headers.get('Cookie'), DB)
	if (verify instanceof Response)
		return new Response(JSON.stringify({'error': 'Unauthorized'}), { headers: { 'Content-Type': 'application/json' }, status: 401 })

	const { id } = verify
	const commentId = params.id
	const { status, comment }: {status?: string, comment?: string} = await request.json()
	const now = new Date().getTime()
	let ret
	if (status === 'deleted') {
		const stmt = await DB.prepare('DELETE FROM comments WHERE id = ?').bind(commentId)
		const { success }: { success: boolean } = await stmt.run()
		if (success)
			ret = {}
	}
	else {
		const rows: [{success: boolean}, {results?: { id: string, member_id: string, name: string, email: string, parent_id: string | null, status: 'published' | 'hidden' | 'deleted', html: string, edited_at: number, created_at: number, update_at: number, like_count: number, liked_by_user: 0 | 1}[]}] = await DB.batch([
			DB.prepare('UPDATE comments SET html = ?1, edited_at = ?2, update_at = ?2 WHERE id = ?3').bind(comment, now, commentId),
			DB.prepare('SELECT c.id, c.member_id, u.name, u.email, c.parent_id, c.status, c.html, c.edited_at, c.created_at, c.update_at, COUNT(DISTINCT cl.id) AS like_count, MAX(CASE WHEN cl.member_id = ?1 THEN 1 ELSE 0 END) AS liked_by_user FROM comments c LEFT JOIN users u ON c.member_id = u.id LEFT JOIN comment_likes cl ON c.id = cl.comment_id WHERE c.id = ?2 GROUP BY c.id').bind(id, commentId)
		])
		ret = (await formatComments(rows[1].results || []))[0]
	}
	return new Response(JSON.stringify(ret), {
		status: 200,
		headers: {
			'Content-Type': 'application/json'
		}
	})
}

async function digestMessage(message: string) {
	const msgUint8 = new TextEncoder().encode(message)
	const hashBuffer = await crypto.subtle.digest('SHA-256', msgUint8)
	const hashArray = Array.from(new Uint8Array(hashBuffer))
	const hashHex = hashArray
		.map((b) => b.toString(16).padStart(2, '0'))
		.join('')
	return hashHex
}
async function formatComments(results: { id: string; member_id: string; avatar_image?: string; replies?: []; name: string; email: string; parent_id: string | null; status: "published" | "hidden" | "deleted"; html: string; edited_at: number; created_at: number; update_at: number; like_count: number; liked_by_user: 0 | 1; }[]) {
	const comments = []
	const replyMap = new Map()
	for await (const comment of results.sort((_, b) => +(b.parent_id === null) || -1)) {
		comment.avatar_image = `https://gravatar.com/avatar/${await digestMessage(comment.email)}?d=blank&s=`
		comment.member_id = await getSHA384Hash(comment.member_id)
		delete (comment as {email?: string}).email
		if (comment.parent_id === null) {
			comment.replies = []
			comments.push(comment)
			replyMap.set(comment.id, comment)
		}
		else
			replyMap.get(comment.parent_id)?.replies?.push(comment) || comments.push(comment)
	}
	return comments
}
```

So that's a lot of code. What do these both do? Let's walk through it.

```ts
// /members/api/member
const { request, locals : { runtime: { env } } } = context
const { DB } = env
const verify = await Verify(request.headers.get('Cookie'), DB)
if (verify instanceof Response)
	return verify
const payload = {
	"$uuid": await getSHA384Hash(verify.id),
	"email": verify.email,
	"$name": verify.name,
	"subscribed": !!verify.subscribed,
	"trackVisits": !!verify.trackVisits
}
return new Response(JSON.stringify(payload), {status: 200})
```

This code grabs the cookies sent from the browser and parses the session id out of it using the `Verify` function. It then returns either a response or information about the user and returns that information. An important thing to note is that I hash the UUID before I return it. This is for an important reason I'll get to later[^1].

```ts
// /members/api/comments/[id]
const { request, params, locals : { runtime: { env } } } = context
const { DB } = env

const verify = await Verify(request.headers.get('Cookie'), DB)
const postId = params.id

const stmt = verify instanceof Response ?
			await DB.prepare('SELECT c.id, c.member_id, u.name, u.email, c.parent_id, c.status, c.html, c.edited_at, c.created_at, c.update_at, COUNT(DISTINCT cl.id) AS like_count FROM comments c LEFT JOIN users u ON c.member_id = u.id LEFT JOIN comment_likes cl ON c.id = cl.comment_id WHERE c.post_id = ? GROUP BY c.id').bind(postId) :
			await DB.prepare('SELECT c.id, c.member_id, u.name, u.email, c.parent_id, c.status, c.html, c.edited_at, c.created_at, c.update_at, COUNT(DISTINCT cl.id) AS like_count, MAX(CASE WHEN cl.member_id = ?1 THEN 1 ELSE 0 END) AS liked_by_user FROM comments c LEFT JOIN users u ON c.member_id = u.id LEFT JOIN comment_likes cl ON c.id = cl.comment_id WHERE c.post_id = ?2 GROUP BY c.id').bind(verify.id, postId)
```

This calls the Verify function, which gets the session id, makes sure it's still valid, and returns either a response or a user. This is then passed to the statement we're preparing. If it's a response, we know this person isn't signed in. This means we just want to get a list of comments and how many likes are on each one. Otherwise, we get *what* comments were made or liked by that user, as well as a list of all comments.

```ts
// /members/api/comments/[id]
const { results }: { results?: {id: string, member_id: string, name: string, email: string, parent_id: string | null, status: 'published' | 'hidden' | 'deleted', html: string, edited_at: number, created_at: number, update_at: number, like_count: number, liked_by_user: 0 | 1}[]} = await stmt.all()
const comments = await formatComments(results || [])
return new Response(JSON.stringify(comments), {
	status: 200,
	headers: {
		'Content-Type': 'application/json'
	}
})
```

Essentially all this code does is just grab the results of the query we constructed in the last step, formats it, and returns it as JSON.

# What's Wrong With This Approach

Every time you load a post, it pings the member endpoint, which does a database query, then it pings the comments endpoint, which does a database query, then it parses all of that and renders all of the content as needed. This is fine for the most part, but would be much easier if it were all done on the server side. With server-side rendering, whenever the server gets a request, it is able to construct the html on the backend before it is sent over. This means I'm not being billed twice for worker requests and I'm not waiting on multiple requests, just one that would be from the same location. This results in improved performance, reduced latency, and better SEO. Here's how this has changed:

```astro
// /src/pages/[slug.astro]
---
const { DB } = Astro.locals.runtime.env
const verify = await Verify(Astro.request.headers.get('Cookie'), DB)
const postId = await getSHA384Hash(slug)
const stmt = verify instanceof Response ?
	await DB.prepare('SELECT c.id, c.member_id, u.name, u.email, c.parent_id, c.status, c.html, c.edited_at, c.created_at, c.update_at, COUNT(DISTINCT cl.id) AS like_count FROM comments c LEFT JOIN users u ON c.member_id = u.id LEFT JOIN comment_likes cl ON c.id = cl.comment_id WHERE c.post_id = ? GROUP BY c.id').bind(postId) :
	await DB.prepare('SELECT c.id, c.member_id, u.name, u.email, c.parent_id, c.status, c.html, c.edited_at, c.created_at, c.update_at, COUNT(DISTINCT cl.id) AS like_count, MAX(CASE WHEN cl.member_id = ?1 THEN 1 ELSE 0 END) AS liked_by_user FROM comments c LEFT JOIN users u ON c.member_id = u.id LEFT JOIN comment_likes cl ON c.id = cl.comment_id WHERE c.post_id = ?2 GROUP BY c.id').bind(verify.id, postId)
const { results }: { results?: {id: string, member_id: string, name: string, email: string, parent_id: string | null, status: 'published' | 'hidden' | 'deleted', html: string, edited_at: number, created_at: number, update_at: number, like_count: number, liked_by_user: 0 | 1}[]} = await stmt.all()
const comments = await formatComments(results || [])
---

<PostLayout>
	...
	<section class="flex flex-col relative">
		<div class="max-w-[calc(56rem+8vw)] w-full mx-auto px-7 sm:px-16 md:px-32 dark:text-light text-dark">
			<Comments comments={comments} slug={slug} client:load />
		</div>
	</section>
</PostLayout>
```

Essentially, this does the database fetching on the server side, which then passes the results as a prop to the comments component, which is still in charge of rendering the response. This also means that nothing has to be exposed to the user and can all be handled seamlessly.

# Challenges: Optimizing Images for Cloudflare Pages

As anyone who has tried to build a website can tell you (or at least good web developers), your images take up a large portion of your bandwidth. The general solution to this is to just use an image CDN, but that costs money and I am a broke college student (which is part of why I use Cloudflare's very generous [free tier for Pages](https://www.cloudflare.com/plans/developer-platform/)). So, I've been using `vite-imagetools` for years, but I wanted to try something different this time and was using Astro's built-in `astro:assets`, which was great and fit exactly what I needed. I could render the lower-sized images at build-time and then import them automatically. Unfortunately, with how this works, the compiler does not know what images will be needed at build time and Cloudflare doesn't have support for node libraries, [such as sharp](https://developers.cloudflare.com/workers/runtime-apis/nodejs/). This means the ordinary workaround of just [rendering the images on demand](https://github.com/withastro/astro/issues/4109) doesn't work either. I need to build it ahead of time. And unfortunately, this is where the facade of this being a nice, simple project ends and the reality of this begins.

# `import.meta.glob`

See, there is a way to get what images are on the server at build time. Simply get a list of all the files in that directory and transform them accordingly. The easiest way to do that is with `import.meta.glob`. So all I have to do is just

```ts
// /src/pages/[slug].astro

pictures = import.meta.glob(['/src/assets/hero/*', '!/src/assets/hero/*.svg'], {
	import: 'default',
	query: {
		w: '447;637;782;904;1012;1200;2000;2800;3440',
		format: `avif;webp;jpg`,
		allowUpscale: true,
		as: 'picture',
		imagetools: true
	}
})
const temp = pictures[image] as () => Promise<{ sources: Record<string, string>; img: { src: string; w: number; h: number; } }>
return await temp()
```

See that query argument? That lets me import them using the width, format, and as that I want to use. However, that also means that it imports that for every file in that directory. I then await the returned function and that gets me the image I want. It's a really roundabout way of doing it, but it works and means I can compress the images at build time, then have a function on the server that returns the correct image when I call it. For anyone else looking to implement something similar, I highly recommend reducing the files you call from the glob. By breaking it down into hero images and inline images and being more selective with my imports, it brought compile times from 40 minutes to 15.

# Why All the Effort?

I'm sure a lot of people at this point would also ask why I'm doing all this with a (primarily) static site framework. In all honesty, I thought the same thing and was part-way through rewriting this entire site in Sveltekit until I realized that most of the changes could be made in Astro and a lot of the challenges I faced would be the same regardless (both Astro and Sveltekit are based on Vite, which has issues with using dynamic routes for importing images). Because of this, I decided to continue using Astro at least for the time being, as I find Astro's mdx support to be better than Sveltekit's [mdsvex](https://mdsvex.pngwn.io/). Also, this may just be due to my lack of experience with it, but I also found Astro's way of doing dynamic routing to be easier than Sveltekit's. Astro has built-in functions to help you generate the routes, whereas Sveltekit has you make your own collections. This offers a lot more flexibility in terms of letting you customize how you collect your content, but is beyond what I need for a simple blog. In addition, Astro has the additional feature of allowing you to import other front-end frameworks as components, which is something I find rather interesting and may use more in the future.

# Conclusion

All of this is to say that when you go to [/migrating-to-ssr-with-astro](/migrating-to-ssr-with-astro) on my site, there are no extra fetch requests after the DOM is loaded, just the initial call and any data needed like images and js, but also means that some of the site functions now work without any JavaScript (try leaving a comment or seeing if you see the member button after you've logged in). While this means that there is more work on the backend for the initial request, it also means there is no waiting for the API to load on the user's end, just the initial fetch. In addition, I'm currently in the process of adding more progressive enhancements to the site so it's mostly functional without any extra resources, making it possible to use all core functionality of the site without any JavaScript enabled.

In this post, we've explored the process of migrating from API endpoints to SSR in Astro. We've also discussed some of the challenges I faced when attempting to optimize images for a Cloudflare Pages site and how I overcame them using custom image compression.

I hope this helps other developers who are considering making the switch to SSR in Astro!

[^1]: One of the things I'm attempting to implement currently is a no-friction unsubscribe link in emails. The idea is that because ideally, you are the only one who has access to that email, we assume that anyone who can get the email has the right to unsubscribe from them. No other changes are allowed to be made without authenticating. The easiest way to do this without creating another database or field for this is to simply use the user id, which is not shown elsewhere, as a known identifier for this. Since it is only ever shown in the emails, that is the only way that anyone would be able to see this. For this reason, I hash it on the backend before I send it over for any information as otherwise, anyone could potentially unsubscribe any other user, specifically by passing the user id of any user to the unsubscribe URL. (I have more notes on this for a future blog post but that's for later).