---
title: Authentik Social Logins with Conditional Invitation Prompt
pubDate: '2024-04-05T00:42:57.892Z'
description: 'A setup of Authentik social logins with a conditional invitation prompt, including Google, Github, Gitlab, and Discord.'
author: 'Josh Stock'
featuredImage: '/src/assets/hero/authentik-homepage.png'
tags: ['authentik', 'homelab', 'security']
---

# The idea
I use [Authentik](https://goauthentik.io/) for all of my self-hosted applications (except one, looking at you [open webUI](https://github.com/open-webui/open-webui)) and am generally very happy with it. That being said, I've been looking into both passwordless and social logins as an alternative option. As it turns out, Authentik is also perfect for that! 

# The passwordless setup
The first step was setting up passwordless logins. Essentially, the idea behind this is to use something to the effect of a WebAuthn device, generally a FIDO2 authenticator, to stand in as your username and password. That being said, you do still need to have a PIN for the device so that someone can't find it and gain access to all of your credentials. Honestly, this was easier than I thought. Authentik has a [write-up](https://docs.goauthentik.io/docs/flow/stages/authenticator_validate/#passwordless-authentication) going over exactly how it works, what issues you may run into, and what it's compatible with. One thing to note, I ran into a small bug in Firefox where if you have that verification is preferable or should not occur, there can be some issue with using a physical key (in my case, a yubikey). If this happens to you, just change it so that user verification must occur and you'll be fine.

That all being said, it works shockingly well. I'm very impressed with just how many things are WebAuthn devices. Currently I have it set up for my Android phone, my Yubikey, and Windows Hello on my desktop and it's all seamless. But I wanted more...

# The social logins
Social logins are also a nice to have. As I invite more people to use tools that I host, I want to make it as easy as possible for them to get started. Everyone I know essentially has a Google account or a Discord so those were my top two priorities. Discord has [official Authentik support](https://docs.goauthentik.io/integrations/sources/discord/), while Google just has [community support](https://docs.goauthentik.io/integrations/sources/google/) but honestly, they both work fine. I also added Github and Gitlab as I know some people who use those services and it's always nice to have options. This was fairly simple to set up, but with a single snag: the invitation prompt.

Invitation prompts are something I absolutely have to have working. With the default access you are given, you can't do any real damage, but I would like to have it that people can't just sign up without me knowing. I do have notifications turned on, but I like the idea of giving someone a one time use invitation and going from there. The issue is that I'd like to have a single invitation prompt that works for both federated and social logins. Essentially, I'd like it to check for the invite token from an invite url and if it doesn't exist, prompts the user for it. I found someone on the Authentik discord who had a similar setup, but I had a few issues with it.

![TIEB62's invitation flow](/src/assets/images/invitation-flow.jpg)

While this works relatively well, it falls apart if you have a one time use token. The issue is that the first invitation stage consumes the token, even if it is not used for anything other than the policy check. If you don't have a need for single use codes, I would recommend this setup as it's more user friendly. If the invitation token in the url isn't valid, it will prompt the user. With the setup that I've made, an invalid invite token would just tell the user that it is invalid or missing. With that being said, here's what I ended up making:

![My enrollment flow](/src/assets/images/enrollment-flow.png)

Essentially, it just checks for a valid itoken parameter by using the following expression policy:

```python
from urllib.parse import unquote

return 'itoken=' in unquote(unquote(request.http_request.META["QUERY_STRING"]))[len('query='):]
```

That being said, it only checks that the invitation parameter exists, not that it's valid. This can cause issues if the token has expired, been copied wrong, or is just plain invalid. I'm still working on a way to check if the token is valid, but I'm not sure if it's possible. If you have any ideas, please let me know! Regardless, it works well enough for my use cases. Hopefully this helps someone else who runs into the same issues I did.