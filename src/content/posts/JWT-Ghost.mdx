---
title: Reverse-Engineering Ghost and Implementing a Custom Blog Engine
pubDate: '2024-04-12T22:54:20.130Z'
description: 'My experience reverse-engineering the front end of the ghost blog engine, their authorization API, and authentication using passwordless.dev'
author: 'Josh Stock'
featuredImage: '/src/assets/hero/passwordless-ghost.png'
tags: ['meta', 'ghost', 'security']
---

So if you've ever met me, you probably know that I don't like the trend of websites getting larger and larger. With the [average webpage being roughly the size of the first Doom](https://www.wired.com/20/src/average-webpage-now-size-original-doom/), I think we should focus on making sites do more with less. Before I spun up this site, I was using Ghost as just a test to see how well I liked it. For what it is, I do like how much time and effort they clearly put into it. The editor is really nice and as a CMS, the ability to have your subscribers sign up and the ability to send out emails in the same application is really nice. However, I'm more than comfortable writing these posts in a text editor or even just VS Code. With that being the case, I wanted to see how much I could replicate without having the admin panel.

## Step 1: The Playbook

The first step was to figure out how much I actually wanted to replicate and what I wanted to make. The first thing I did was play around with Ghost and see how much of it the end user was able to use or see. Outside of the subscription button and membership tiers, there wasn't a lot I necessarily needed to replicate. The main features I was interested in were:
1. A mailing list
2. User authentication via magic links (this will also serve as an easy way to verify their email address)
3. User authorization via JWT
4. User management via an easy to use interface

## Step 2: Magic Links

While this would be (somewhat) simple to create from scratch, Bitwarden already has a free product that does exactly what I need: [Passwordless.dev](https://bitwarden.com/products/passwordless/). Their flow for a magic link is very simple, requiring a uuid, an email address, and a callback url. This means I don't have to deal with mailing features just yet, but gave me another issue. They state many times that the email addresses are not stored on their servers, they simply pass along the email with the authentication token for you to verify. This means I am responsible for storing the email addresses and the security of that. This is relatively simple to deal with using [Cloudflare's D1 databases](https://www.cloudflare.com/developer-platfo/src/). They're globally replicated so response times are incredibly quick and it's already tied into the Cloudflare account I'm hosting this site on.

```typescript
const UUID = crypto.randomUUID()
const { success } = await db.prepare('INSERT INTO users (id, name, email, subscribed, verified, emailUUID, newEmail) VALUES (?, ?, ?, ?, ?, ?, ?)').bind(UUID, name, email, true, false, null, null).run()
if (!success)
	return new Response(JSON.stringify({'error': 'Error'}), {status: 500})
return sendLink(email, UUID, referrer, api)
```

And that's it. That's the entire flow for signing up a user. A unique ID is generated and associated with an email address and a name. This is then added to the database and, assuming it worked, is sent off to Bitwarden to take care of authentication. The user will receive an email similar to this:

![Passwordless email](/src/assets/images/passwordless-email.png)

And because by the time this post has gone up, that url is no longer valid, I don't have to worry about censoring any of it. It simply takes you to the route that will verify that token with Bitwarden, which will return a success value, the user ID associated with that token, a timestamp, and expiration time. From here, I verify that the user ID does indeed exist and from there I can start signing the token. Also, the first time you visit this endpoint, it verifies the account. That way I won't send email to anyone who hasn't verified that they own that email address.

## Step 3: JWT

Another thing you probably know about me is that I like to learn things even when there's a really good way to do it already. Following up on [my last post](/authentik-setup), I've been very interested in authentication and authorization and this seemed like a very good excuse to learn more about how JWTs worked. If you don't know, I highly recommend [Auth0's information](https://jwt.io/) on the subject. The best and most useful part was the debugger on there. Essentially, the entire JWT is just a simple way of encoding information about the user and signing it so you can verify the information was made by you. To pick up from the last step, we've verified that the user ID exists and that the token they provided is valid.  From there, it gets the Issued At Time, counted in seconds since the epoch, the EXPiration time, counted the same way, the ISSuer, which is the url associated with the token, the AUDience, which is the UUID for the user, the SUBject, which in this case is the token ID that was returned by Bitwarden, packs that all up, and signs it using HMAC-384. It bundles that up and sends that to your browser as a cookie.

```typescript
const iat = new Date(timestamp).getTime() / 1000
const exp = new Date(expiresAt).getTime() / 1000
const iss = 'https://blog.joshuastock.net'
const aud = userId
const sub = tokenId
const JWT = await Sign({ sub, iat, exp, name, aud, iss }, secret)
const payload = `sessionId=${JWT}; Path=/; Expires=${new Date(expiresAt).toUTCString()}; SameSite=lax; Secure; HttpOnly=true`
```

## Step 3.5: Verification and Authorization

Obviously all of that isn't much use if we're not verifying that you have access to do what you are trying to do. This is where that JWT comes in. You may have noticed that when you visit this page, there's a small delay between when the page loads and when the subscription button loads. That's because it's checking to see who you are and if it should show the membership information for you. For this part, I can actually just show you the whole code because of how short it is.

> Edit: I have since been made aware that you should not verify the token by directly comparing them due to timing attacks. This has been fixed in the current version of the site and will be updated in a future post.

```typescript
async function verify(cookies: string, secret: string): Promise<Response | { aud: string }> {
	// Cookies are stored as just one long string and we just need the sessionId one
	const JWT = cookies?.split('; ').find(cookie => cookie.startsWith('sessionId'))?.split('=')[1]
	// If it doesn't exist, there's nothing to return
	if (!JWT)
		return new Response(null, {status: 204})
	// All the values here are the header, payload, and signature respectively
	const vals = JWT.split('.')
	// The first thing we want is to parse the payload. unBase just converts it from base64 to a normal string
	const { sub, iat, exp, name, aud, iss } = JSON.parse(unBase(vals[1]))
	// If I didn't issue it, I don't want it
	if (iss !== 'https://blog.joshuastock.net')
		return new Response('Invalid token', {status: 400})
	const now = new Date().getTime() / 1000
	// If you got this token more than an hour ago, I don't want it either
	if (exp < now)
		return new Response('Token expired', {status: 400})
	// This part shouldn't even be possible so I definitely don't want a time traveler to access anything
	if (iat > now)
		return new Response('Token not yet valid', {status: 400})
	// As a final last step, we attempt to sign the token using all of the same information
	const verify = await Sign({ sub, iat, exp, name, aud, iss }, secret)
	// If we don't get the same exact answer, that means someone else is trying to sign it and pass it off as their own
	if (JWT !== verify)
		return new Response('Invalid token', {status: 400})
	// If all of these steps passed, it's genuine and we can keep going
	return { aud }
}

export async function GET(context: { locals: { runtime: { env: { DB: any, API: string, SECRET: string } } }; request: Request }) {
	// Here, we get the request and the cookies associated with it (if there are any)
	const request = context.request
	const cookies = request.headers.get('Cookie') ?? ''
	const db = context.locals.runtime.env.DB
	const secret = context.locals.runtime.env.SECRET
	// Down here, we verify that the cookie is signed correctly by calling the verify function
	const aud = await verify(cookies, secret)
	// If the cookie isn't valid for whatever reason, it sends a response so we simply return that back to them
	if (aud instanceof Response)
		return aud
	// Otherwise, we'll find the users in the database that match the UUID in the cookie
	const stmt = await db.prepare('SELECT * FROM users WHERE id = ?').bind(aud.aud)
	const { results } = await stmt.all()
	// If there are no results, you don't exist and we wouldn't want you to be able to do anything on this site
	if (results.length < 1)
		return new Response('User not found', {status: 404})
	// If there are, we grab the information about you so that you can modify any of the information
	const { id, email, name, subscribed } = results[0]
	const payload = {
		"uuid": id,
		email,
		name,
		// A small issue is that Cloudflare stores their booleans as integers. We just have to negate it twice to turn it back into a boolean
		"subscribed": !!subscribed
	}
	// Take all of that, package it up, and return an ok response
	return new Response(JSON.stringify(payload), {status: 200})
}
```

## Step 4: User management

Now that all of that's out of the way, we know who you are and that you are in fact that person. Based on this, we'll let you modify some portions of the user interface. Here's what mine looks like

![Account settings](/src/assets/images/account-settings.png)

Pretty neat, right? It looks really similar to Ghost's interface without using any of their code. In fact, it's written entirely using tailwind classes. From here, you're able to change your name, email address, or newsletter preference. If you have a [Gravatar account](https://gravatar.com/) associated with it, it'll even grab your profile picture.

Side note, I really like how Gravatar does that. They have the url for your profile picture be a SHA-256 hash of your email address. It's a neat little way so they're not just willingly given any user's email address. I mean, looking at it there really isn't another way they should do it but still, it's nice. Reminds me of how [HIBP](https://haveibeenpwned.com/) checks for password breaches.

Anyway, from here any changes are sent to the API endpoint, which does the same verification checks on the JWT every time it gets another request. The only thing that's a little unique is

## Step 5: Email changes

Man, this was annoying at first. Remember how I said that Bitwarden doesn't store emails? Yeah, they were not kidding about that. Even if you set an alias for a user, there is no way for anyone else to know what that alias is so I can't check emails that way. It really is just your user ID that ties you to this. So, this has a small change.

If you want to change your email address, the main concern is that it's a valid email and that you own it. The former is easy to check. Input type on the front end, regex on the backend. But verifying you own it was slightly more difficult at first. Because I need to make sure you own that, I have to send a magic link. But I won't know what email it was sent to unless I put that in the url, which would be an issue because the user can just change that on their own. Here's what I ended up doing:

```typescript
async function updateEmail(name: string, newEmail: string, aud: string, db: any, api: string, referrer: string) {
	const UUID = crypto.randomUUID()
	const stmt = await db.prepare('UPDATE users SET name = ?, newEmail = ?, emailUUID = ? WHERE id = ?').bind(name, newEmail, UUID, aud)
	const { success } = await stmt.run()
	if (!success)
		return new Response(JSON.stringify({'error': 'Error updating email'}), {status: 500})
	return sendLink(newEmail, aud, referrer, api, {'uuid': UUID})
}
```

See what I mean? Essentially, whenever someone has an intent to change their email, I generate a unique ID for that email address, then store both in the database. In the magic link, I include the UUID as part of the parameters there. Finally, when I check the url to give you the JWT, I check for a UUID parameter. If it's a valid UUID *and* matches the one in the database, I update the email address. The reasoning for this is that if you start to change the email, then never click the link, I don't want it to change the next time you sign in either. It's a pain but it does work and would be difficult to get around. Oh, I should also mention - because the link in the email is only valid for 1 hour, the UUID is too. Even if you were to change the email address to the same one again, it would generate a different unique ID and would clear it on that sign up so you can't accidentally set it after the fact.

## Step 6: This post

That's essentially it. I just wanted to give an update for how it works. I still need to write the code that will send out a newsletter but that should be a simple cronjob and HTML email template. I will say, I am primarily a backend person, not a frontend one so there may be changes coming up relatively soon. For one, it would probably be nice to see an "Email sent" message or quite literally anything other than the lack of an error when signing up. But that's for another day. Because this entire site is [open source](https://gitlab.com/1337_Nerd/blog), you can see how just getting this far has taken several days and multiple commits. But, I like to think it's worthwhile. Hopefully this helps someone else who doesn't want to pay for hosting Ghost or just wants something that's roughly 1/10th of the size of the default Ghost page.