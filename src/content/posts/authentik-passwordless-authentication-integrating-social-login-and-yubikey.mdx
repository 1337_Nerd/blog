---
title: 'Authentik passwordless authentication: Integrating social login and YubiKey'
pubDate: '2024-07-30T15:24:18.162Z'
description: 'Learn how to integrate social media platforms and YubiKey devices with Authentik, a self-hosted Identity Provider. Get step-by-step guides on setting up passwordless authentication, troubleshooting tips, and best practices for enhanced security.'
author: 'Josh Stock'
featuredImage: '/src/assets/hero/franck-DoWZMPZ-M9s-unsplash.jpg'
tags: ['authentik', 'homelab', 'security', 'yubikey', 'fido2', 'webauthn']
---

# Introduction

One of the features I use most often in my Authentik setup is passwordless authentication. With this, I can use any form of biometric login on any of my devices, whether that be my Android phone or my Windows laptop through Windows Hello. This makes it much easier to use than having to type in a username and password, as well as making it more secure. Passwordless authentication is more secure by default as it is built with multi-factor authentication in mind, as well as being phish-resistant. During my time at Marathon Petroleum, this was one of our goals, to reduce the amount of phishing attacks that we received as well as making sure that we had phish-resistant authentication. Due to this, I wanted to have a more convenient and secure way for me to login through my own system.

# Social Login

## Benefits

What's the best way to host passwords? To not host them at all! That's the general idea behind social logins. Theoretically, companies like Google, Discord, GitHub, and GitLab (the providers I use), presumable have much better security than a college student would and have the ability to throw money at problems to make them more secure. The idea behind social logins is that you piggyback off of their authentication flow by using (generally) OAuth2 or OpenID Connect. When you start the login flow on Authentik's end, it redirects to the provider's, in this case, Google's, authentication and authorization flow. If you've ever gone to a page and seen a "Use your Google Account to sign in to [website]", that's a social login prompt.

![Love or hate it, these prompts are everywhere](/src/assets/images/google-social-login.png)

Upon completing authentication, you are redirected back to the original IdP, in this case, Authentik, which verifies the response and authenticates you. Now, there's a lot more detail we could go into, but that's beyond the scope of this post. In addition, [you may want to reconsider blindly trusting social logins](https://salt.security/blog/over-1-million-websites-are-at-risk-of-sensitive-information-leakage---xss-is-dead-long-live-xss). Just make sure you have sensible protections in place and you should be fine, but it is something to be aware of. However, for most people, this is preferable compared to the idea of hosting their own authentication flow, even if it is built on an existing framework.

## Setup Process

By now you should have a good idea what the benefits of social logins are and why you would want to set it up. If you've decided it's not for you, keep on using Authentik as you have been. If you'd like to see a real-world example of how I personally have it set up, keep reading.

While Authentik does have [their own documentation](https://docs.goauthentik.io/docs/sources/google/) for multiple providers, I've personally found it somewhat lacking. While a large portion of the setup on the Google side has been documented fairly well, it seems to be missing some key details for how to set it up on the Authentik side. Here I have guides for [Google](#google), [Discord](#discord), [GitHub](#github), and [GitLab](#gitlab), as well as a way to [add social login prompts to the default login page](#adding-sources-to-default-login).

### Google

To start, go to the [Google Cloud Console](https://console.developers.google.com/) and create a new project if you haven't already.

![Create a new project](/src/assets/images/google-console-project-creation.png)

Set the project name to whatever helps you easily identify it and set the organization. Just leave the organization blank if you do not have one.

After the project has been created, select the "Credentials" menu item on the left.

![Google Cloud Console sidebar](/src/assets/images/google-console-sidebar.png)

From here, you will need to create an OAuth consent screen. This essentially states to the user what information and permissions are shared with Authentik. This is because you can request several "scopes" with it. A scope is essentially a certain permission. For example, a project may want to be able to read and write emails as if they are the user. This would require them to request those permissions and the consent page is so the user is aware of what permissions are being allowed, in case they did not want an unknown company to be able to send emails on their behalf.

![Example consent screen](/src/assets/images/heimdall_consent_screen.png)

From here, you have the option of selecting "Internal" or "External" for the user type. If you don't have a GSuite account, select external and continue. If you do have a GSuite account, internal is used if you want only users within that organization to be able to authenticate. I will be going along with external for the time being.

![Google Cloud Console Consent Screen options](/src/assets/images/google-console-consent-screen.png)

From here, there are only a few required fields. For simplicity, these are the only ones that are *needed* for the application to work:

1. App name
2. User support email
3. Authorized domains. While this is not *required*, you cannot use Google's login with a URL that has not already been specified here. **If you do not set this your Authentik setup will not work through Google.**
4. Developer contact information

After this, you will be prompted for additional scopes. If you do not have any or do not know what this means, you can skip this step. Google adds the defaults needed for authorization to work.

Finally, it will show the summary of what you have set up. From here, you are done with the consent screen. Go back to "Credentials" on the left of the screen and select "Create Credentials," then "OAuth Client ID." The application type is "Web application," the name can be whatever helps you identify it, and set the "Authorized redirect URIs" to `https://authentik.company/source/oauth/callback/google/`. In my case, this is `https://authentik.joshuastock.net/source/oauth/callback/google/`. If you are unsure what yours is, you can view this within Authentik as you set up the social login.

After saving, there will be a prompt showing the client ID and client secret. Make sure to copy these for the setup within Authentik.

#### Authentik setup

This configuration is valid based on version 2024.4.3.

From the Authentik admin panel, go to Directory > Federation and Social login, then click "Create." Select "Google OAuth Source" and follow the setup as you would for any other application. Set the name and slug to whatever your preferred name is, then set the "Consumer Key" to the client ID you got from the last step and set the "Consumer Secret" to the client secret. Click "Finish" and you're done! If you want to display the new social login prompt on the login page, skip to [adding sources to default login](#adding-sources-to-default-login).

### Discord

To start, go to the [Discord Developer Portal](https://discord.com/developers/applications) and create an application, naming it whatever helps you identify it.

![Discord Developer Portal](/src/assets/images/discord-application.png)

From here, select "OAuth2" from the sidebar.

![Discord Developer Sidebar](/src/assets/images/discord-sidebar.png)

Copy the client ID and reset the secret key to generate a new one. Discord may ask you to provide an MFA (multi-factor authentication) code. Copy the secret as well, we will use this in the next step.

Make sure to add a redirect in the form of `https://authentik.company/source/oauth/callback/discord/`. In my case, this would be `https://authentik.joshuastock.net/source/oauth/callback/discord/`

![Discord OAuth2 Settings (these are fake values)](/src/assets/images/discord-oauth2.png)

#### Authentik setup

This configuration is valid based on version 2024.4.3.

From the Authentik admin panel, go to Directory > Federation and Social login, then click "Create." Select "Discord OAuth Source" and follow the setup as you would for any other application. Set the name and slug to whatever your preferred name is, then set the "Consumer Key" to the client ID you got from the last step and set the "Consumer Secret" to the client secret. Click "Finish" and you're done! If you want to display the new social login prompt on the login page, skip to [adding sources to default login](#adding-sources-to-default-login).

For more advanced configuration, you can add expression policies to check for being in specific guilds (servers) or specific roles within those guilds. This is beyond the scope of this guide, but Authentik has a write-up [available here](https://docs.goauthentik.io/docs/sources/discord/#checking-for-membership-of-a-discord-guild).

### GitHub

Create an OAuth App from [GitHub's Developer Settings](https://github.com/settings/developers). Give it a name (whatever helps you identify it), a homepage URL in the form of `https://authentik.company`, so in my case `https://authentik.joshuastock.net`, and a callback URL in the form of `https://authentik.company/source/oauth/callback/github` (`https://authentik.joshuastock.net/source/oauth/callback/github` in my case). You can choose to allow device flow authorization, but this is only used for headless applications (ones without a GUI like any command line application).

![GitHub OAuth App Creation](/src/assets/images/github-oauth.png)

From here, you will need to copy the client ID and generate a new client secret. Copy both of these, as you will use them in the next step.

#### Authentik setup

This configuration is valid based on version 2024.4.3.

From the Authentik admin panel, go to Directory > Federation and Social login, then click "Create." Select "GitHub OAuth Source" and follow the setup as you would for any other application. Set the name and slug to whatever your preferred name is, then set the "Consumer Key" to the client ID you got from the last step and set the "Consumer Secret" to the client secret. Click "Finish" and you're done! If you want to display the new social login prompt on the login page, skip to [adding sources to default login](#adding-sources-to-default-login).

### GitLab

While there is no documentation for this on Authentik's homepage, the setup is simple enough and GitLab has [its own documentation on a generic setup](https://docs.gitlab.com/ee/integration/oauth_provider.html). I will be going forward with this guide under the assumption that you are using gitlab.com as your GitLab instance. If you are hosting your own, replace that with your instance URL.

First, go to [your applications settings page](https://gitlab.com/-/user_settings/applications) and select "Add new application." Give it a name that will help you identify it, as well as a redirect URI in the form of `https://authentik.company/source/oauth/callback/gitlab/`, which is `https://authentik.joshuastock.net/source/oauth/callback/gitlab/` in my case. Check "Confidential," as we will be using a client secret. The only scopes you need to select are "openid", "profile", and "email." From here, save the application and copy the application ID and secret, which will be used in the next step.

![GitLab application summary](/src/assets/images/gitlab-application-summary.png)

#### Authentik setup

This configuration is valid based on version 2024.4.3.

From the Authentik admin panel, go to Directory > Federation and Social login, then click "Create." Select "GitLab OAuth Source" and follow the setup as you would for any other application. Set the name and slug to whatever your preferred name is, then set the "Consumer Key" to the client ID you got from the last step and set the "Consumer Secret" to the client secret. If you are using your own instance, expand the "URL Settings" section and change the URLs. By default, this is set to `gitlab.com`. After this, click "Finish" and you're done! If you want to display the new social login prompt on the login page, skip to [adding sources to default login](#adding-sources-to-default-login).

## Adding Sources to Default Login

So you've set up social logins, but don't have a way to actually select it. To do this, all you need to do is go to the Authentik admin panel, then Flows > default-authentication-flow (or whatever your authentication flow is) > Stage Bindings. Select "Edit Stage" for your identification stage and expand "Source Settings." You should see a prompt similar to this:

![Authentik Source Settings](/src/assets/images/source-settings.png)

Select which ones you want and they should appear on the login page.

## Connect Social Logins to Existing Account

This is a small quality-of-life improvement, but you can link your existing Authentik account to a social login. This is useful if you want to sign in using either your Google, Discord, GitHub, GitLab or whatever social logins you have set up as well as your default Authentik account. To set this up, go to the user settings page and then to "Connected Services." From here, there will be a prompt similar to the one below.

![User connected services selector](/src/assets/images/user-connected-services.png)

Follow the prompts and you will be able to link your existing federated logins with your Authentik account, allowing you to use either.

## Appearance Tips

This is very minor, but I do like to add icons for each social login. You can do this either when creating the social login or you can always edit it after the fact. A small note, make sure you select a pure black image (in my case I use svgs), as it will invert the colors in dark mode, which can cause it to look bad, especially if the icon has several different colors.

# Passwordless Authentication

If you have any WebAuthn authenticators set up as a 2FA option in Authentik, you also have the ability to have that be your sole source of authentication. No username, no password, no nothing other than your WebAuthn device. This is also arguably more secure than using a username and password, as it fulfills the requirements of [something you have and something you are](https://pages.nist.gov/800-63-3/sp800-63b.html#421-permitted-authenticator-types). In addition, Authentik requires user verification. This means that it requires your PIN for a YubiKey (or any other U2F key) or for you to enter your password for Windows Hello. There's more detailed information in [this article](https://web.dev/articles/webauthn-user-verification), but essentially, user verification validates that the user is present and has to reauthenticate through the WebAuthn device. If you are using biometrics though, that is considered authentication so you only need to prove your identity that way. This reduces friction for logins, while also making it more secure, as you have multi-factor authentication built into the flow.

## Setup

As before, Authentik has [their own documentation](https://docs.goauthentik.io/docs/flow/stages/authenticator_validate/#passwordless-authentication), but I've found it somewhat lacking. In addition, they only very briefly mention some bugs I ran into. The main issue I had is that you *must* set WebAuthn User verification to "User verification must occur." If this is not set, you run into issues with some builds of Firefox, as while Chrome defaults to preferring verification, Firefox does not (in some cases), which will cause passwordless authentication to fail on devices like YubiKeys, as they are not prompted to ask the user for the PIN.

For simplicity, I have a yaml config file for the passwordless authentication flow you need to set up initially. 

```yaml
# passwordless-authentication.yaml
context: {}
entries:
- attrs:
    authentication: none
    compatibility_mode: true
    denied_action: message_continue
    designation: authentication
    layout: stacked
    name: Passwordless Authentication
    policy_engine_mode: any
    title: Passwordless authentication
  conditions: []
  identifiers:
    pk: 8ad8f48b-aa2a-43b9-a6d4-d620e337d974
    slug: passwordless-authentication
  model: authentik_flows.flow
  state: present
- attrs:
    device_classes:
    - webauthn
    last_auth_threshold: seconds=0
    not_configured_action: deny
    webauthn_user_verification: required
  conditions: []
  identifiers:
    name: WebAuthn Validation Stage
    pk: aabca658-696d-4b7d-8dfd-503646a1903b
  model: authentik_stages_authenticator_validate.authenticatorvalidatestage
  state: present
- attrs:
    geoip_binding: bind_continent_country_city
    network_binding: bind_asn
    remember_me_offset: seconds=0
    session_duration: hours=12
  conditions: []
  identifiers:
    name: default-authentication-login
    pk: a2c7d1c6-2a12-43b7-99c3-73ede742fe58
  model: authentik_stages_user_login.userloginstage
  state: present
- attrs:
    invalid_response_action: retry
    policy_engine_mode: any
    re_evaluate_policies: true
  conditions: []
  identifiers:
    order: 0
    pk: 7046f22b-1675-4c24-9938-70fdcfb15dc9
    stage: aabca658-696d-4b7d-8dfd-503646a1903b
    target: 8ad8f48b-aa2a-43b9-a6d4-d620e337d974
  model: authentik_flows.flowstagebinding
  state: present
- attrs:
    invalid_response_action: retry
    policy_engine_mode: any
    re_evaluate_policies: true
  conditions: []
  identifiers:
    order: 10
    pk: e31b7417-6cfc-4e45-aab1-aa7f8c766c0d
    stage: a2c7d1c6-2a12-43b7-99c3-73ede742fe58
    target: 8ad8f48b-aa2a-43b9-a6d4-d620e337d974
  model: authentik_flows.flowstagebinding
  state: present
metadata:
  labels:
    blueprints.goauthentik.io/generated: 'true'
  name: authentik Export - 2024-07-30 13:26:37.737068+00:00
version: 1
```

This is the new flow that will need to be created. In addition, I would recommend editing the default-authentication-login stage, as I have some rather restrictive defaults on network and GeoIP binding. That being said, feel free to import the file as is, just know that it may revoke the session more frequently. After you have done this, go to your `default-authentication-flow` or whatever authentication flow you want to set this up on. From here, go to Stage Bindings, then select "Edit Stage" for your identification stage. Under "Flow settings," you should see an option for "Passwordless flow." Select the new `passwordless-authentication` flow we just created and update the stage.

If all is set up correctly, you should see a new option on the login page that says "Use a security key." Clicking this will launch the passwordless authentication flow, bypassing the rest of the identification stage. An important note: this only works with WebAuthn authenticators. If you would like to use Duo for passwordless authentication, Authentik does have a way to set it up with a [custom policy](https://docs.goauthentik.io/docs/flow/stages/password/#passwordless-login), but that is beyond the scope of this post.

# Conclusion

Hopefully, this post will help others set up Authentik with social login and passwordless authentication. I truly believe that both of these are essential to using Authentik if for nothing other than ease of use and better security. The only issue I've had with this setup is using single-use invitations with social login signups. For that, I direct you to my post showing [my solution to this supremely first-world problem](/authentik-setup). If you have any questions about the setup, feel free to reach out to me or comment below!