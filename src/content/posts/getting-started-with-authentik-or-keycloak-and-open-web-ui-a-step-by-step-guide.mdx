---
title: "Getting Started with Authentik or Keycloak with Open WebUI: A Step-by-Step Guide"
pubDate: '2024-08-02T03:02:34.915Z'
description: "Learn how to set up Authentik or Keycloak with Open WebUI for passwordless authentication. Discover the benefits of self-hosted IdPs like Authentik, and explore alternatives like Keycloak."
author: 'Josh Stock'
featuredImage: '/src/assets/hero/open-webui-splash.png'
tags: ['meta', 'security', 'authentik', 'openwebui', 'idp', 'keycloak']
---

# Introduction

As I've mentioned in [multiple](/authentik-setup) [posts](/authentik-passwordless-authentication-integrating-social-login-and-yubikey) in the past, I use Authentik as my personal IdP. One of those things I have recently started using it for is with [Open WebUI](https://github.com/open-webui/open-webui). This is a self-hosted LLM user interface that makes using LLMs (large language models) much easier for the average person, as well as having additional features like web scraping, searching for content on the internet, and (very limited) support for tooling. As of version [v0.3.6](https://github.com/open-webui/open-webui/releases/tag/v0.3.6), there is also experimental support for using OAuth2 for authentication. While there are a number of tutorials for how to set up authentication with [Google](https://support.google.com/cloud/answer/6158849) or [Microsoft](https://learn.microsoft.com/en-us/entra/identity-platform/quickstart-register-app), I have not seen any for generic OIDC setups like [Authentik](https://goauthentik.io/) or [Keycloak](https://www.keycloak.org/).

# Prerequisites

Before diving into the setup process, ensure you have the following components ready:

1. **Open WebUI**: Make sure you have Open WebUI installed and running. You should be using version v0.3.6 or later, as OAuth2 support was introduced in this version.
2. **Identity Provider**: You'll need either Authentik or Keycloak (or another IdP) set up and accessible. If you haven't installed these yet, refer to their respective documentation:
    - [Authentik Installation Guide](https://docs.goauthentik.io/docs/installation/)
    - [Keycloak Installation Guide](https://www.keycloak.org/guides#getting-started)
3. Network Access: Ensure that Open WebUI can reach your IdP server and vice versa. This might involve configuring firewall rules or setting up proper DNS resolution if you're using domain names.
4. SSL Certificates: For security reasons, it's highly recommended to use HTTPS for both your IdP and Open WebUI. Make sure you have valid SSL certificates set up for both services.

# Setup

## Authentik

The first step is to create a new application in Authentik. I am on version 2024.4.3, but the interface has not changed drastically in a while so this information should be accurate for a while. Go to Applications > Applications, then "Create With Wizard."

![Create with Wizard](/src/assets/images/authentik-create.png)

From here, you will create an application like you normally would. Choose a name that's descriptive to you, as well as any groups you may want to limit access to.

![Authentik application creation](/src/assets/images/authentik-application.png)

Select "OAuth2/OIDC" for the provider type.

![Authentik provider type](/src/assets/images/authentik-provider.png)

Select an authorization flow (either implicit or explicit, depending on whether you want to prompt for consent to use the required scopes, as well as set the client type to "Confidential."

![Authentik provider configuration](/src/assets/images/authentik-provider-config.png)

At this point, copy the client ID and client secret for use later, then submit.

## Open WebUI

While [Open WebUI has its own documentation](https://docs.openwebui.com/tutorial/sso/#oidc), I've found it to be very sorely lacking. Below is my `.env` file, which I import into the docker-compose `.yaml` config.

```txt
# .env
OAUTH_CLIENT_ID=BBoCyAcGbgmy6SU73OBECM6vvB9IvHAHaH85iEMO
OAUTH_CLIENT_SECRET=jZga4mBovHWEhGIYqBwsZ72M0rIbgccGJScGR8KS8mk1rckpFODsve6jcHOjEafgzyPzF6q3jS5rcxxe4IwOWG35x4euZySuVXwR04Vm46Yuf1QqZgzKWw7aGYZaeJJQ
OPENID_PROVIDER_URL=https://authentik.joshuastock.net/application/o/open-webui/.well-known/openid-configuration
OAUTH_PROVIDER_NAME=authentik
ENABLE_OAUTH_SIGNUP=true
```

Neither of these are the actual client ID or client secret. But this is roughly what you would set up. The `OPENID_PROVIDER_URL` is in the form of `https://[authentik.domain]/application/o/[application/.well-known/openid-configuration`. Do note that while enabling signup is optional, that means that you would have to explicitly create the user in both your IdP and Open WebUI to allow them to sign in. Personally, I lock down restrictions on Authentik's side so I know that the only people who would use it have the right groups or roles to access it. After you set these environment variables, restart the container and you should see a button on login that looks like this:

![Open WebUI sign-in prompt](/src/assets/images/open-webui-sign-in.png)

After the first successful redirect is used, the redirect URI is automatically updated in Authentik. That being said, if you are testing Open WebUI locally and then switch to exposing the service, the redirect will not work as it is expecting it to resolve to a local IP. If you use both, I would recommend adding a new line for the redirect URI like so:

![Authentik redirect URIs](/src/assets/images/authentik-open-webui-redirect-uri.png)

## Bonus: Keycloak Setup

While I do not personally use Keycloak, I recognize that it is arguably the most popular self-hosted IdP, with large sites like [Red Hat](https://sso.redhat.com/) and [MangaDex](https://auth.mangadex.org/) using Keycloak. Because of this, it only seems fair that I write how to set up Keycloak for use with Open WebUI.

Because I am setting Keycloak up solely for this demo, I am using the default Keycloak realm, which has the id of "master." Assuming you use this as your main IdP, you will most likely want to put this in a different realm. For the configuration options here, substitute `master` with the name of your realm.

To start, go to Clients and create "Create client." One main difference between Authentik and Keycloak is that you choose your own client ID. In this case, I have made it open-webui. Make sure to select "OpenID Connect" as the client type.

![Keycloak create client](/src/assets/images/keycloak-create-client.png)

In the capability configuration section, toggle "Client authorization" to on, then hit next.

![Keycloak capability config](/src/assets/images/keycloak-capability-config.png)

At this point, the only setting you need to change is the "Valid redirect URIs." While you may want to set the root URL or home URL for use within Keycloak, it is optional. An important note is that, unlike Authentik, you have to explicitly set the valid redirect URIs, they will not be inferred on the first login.

![Keycloak login settings](/src/assets/images/keycloak-login-settings.png)

At this point, save the client ID you defined, as well as the client secret located under "Credentials" and we are done with our setup within Keycloak and can move on to Open WebUI. Go to your `.env` file and make the following changes:

```txt
OAUTH_CLIENT_ID=open-webui
OAUTH_CLIENT_SECRET=0ccA7Tufq0OaYLvP06OxDG4mdA0KmafL
OPENID_PROVIDER_URL=https://keycloak.joshuastock.net/realms/master/.well-known/openid-configuration
OAUTH_PROVIDER_NAME=Keycloak
ENABLE_OAUTH_SIGNUP=true
```

Replacing your client ID and client secret with the variables within Keycloak. Also, the `OPENID_PROVIDER_URL` is in the form of `https://[keycloak.domain]/realms/[realm]/.well-known/openid-configuration`, which you can also find on Keycloak under Realm settings > General on the left-hand side of the screen, at the bottom of the page where it says "OpenID Endpoint Configuration."

![Keycloak OpenID Endpoint](/src/assets/images/keycloak-realm-settings.png)

After this, restart the Open WebUI container and you should be good to go! One small issue I ran into is that if you do not have an email address set in Keycloak for your user, it will fail to grab the correct scopes.

# Benefits of Using SSO

1. **Convenience**: With OIDC, users only need to remember one set of credentials (e.g., username and password) across multiple applications, reducing the administrative burden and allowing users to access other services I host, if they allow it.
2. **Security**: OIDC provides a standardized, secure way to authenticate users, utilizing industry-accepted protocols like OAuth 2.0 and JSON Web Tokens (JWT). This ensures that data is defined in an easy-to-use scope and allows more granular control over user administration.
3. **Scalability**: As the number of applications and services grows, OIDC simplifies the authentication process, making it easier to manage user identities across multiple platforms.
4. **Two-Factor Authentication (2FA)**: Many OIDC providers offer built-in 2FA capabilities, adding an extra layer of security to the authentication process. Authentik and Keycloak can also provide options where 2FA is required for certain roles and groups.
5. **Role-Based Access Controls (RBAC)**: With OIDC, you can define roles and permissions for users, ensuring that they only have access to the resources and features they need.
6. SCIM (System for Cross-domain Identity Management): Some OIDC providers offer SCIM support, enabling the management of user identities across multiple applications and services. This is true for both Authentik and Keycloak, though they may be paid features.

# Conclusion

While I know it can be a massive undertaking to set up an IdP that you host yourself, it can also be very beneficial. I have one login for all of my services, with benefits such as [passwordless login](/authentik-passwordless-authentication-integrating-social-login-and-yubikey), 2FA, and failed login detection and automatic banning. I hope I've convinced some people to set up their own IdP for use with Open WebUI, as it makes it much easier to use daily. While I personally feel that Authentik is much easier to use, Keycloak is also a great choice for setting up SSO with your self-hosted services. Whether you're just getting started with OIDC providers or just want to set it up with Open WebUI, I hope this guide has helped you. Don't hesitate to reach out if you have any questions or need further assistance - and happy authenticating!