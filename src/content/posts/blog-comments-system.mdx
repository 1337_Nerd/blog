---
title: 'From Utterances to Awesome: How I Built a Custom Comment Engine with Svelte and Astro'
pubDate: '2024-06-30T02:39:41.965Z'
description: "Say goodbye to limitations and hello to customization! This post shows you how to replace Utterances with a custom comment system built using Svelte and Astro, giving you complete control over your blog's commenting experience."
author: 'Josh Stock'
featuredImage: '/src/assets/hero/custom-comments.png'
tags: ['meta', 'utterances']
---

# Introduction

I recently replaced Utterances, my previous commenting system, with a custom implementation built using Svelte and Astro. This decision has given me complete control over the look and feel of my comments section, allowing me to customize the layout, add or remove features as needed, and even integrate other APIs to enhance the user experience.

# The Problem with Utterances

Utterances is a popular choice for adding comments to a blog. It's a GitHub issue-based commenting system that's easy to set up and use. However, it has some limitations that I found frustrating. Mainly, it requires users to have a GitHub account to comment. This is a barrier to entry that I wanted to avoid. I wanted to make it as easy as possible for people to comment on my blog posts. I also wanted to have more control over the design and functionality of the comment system.

# The Solution: Astro and Svelte

To address this problem, I turned to Svelte and Astro - two powerful JavaScript frameworks that allow me to build a custom commenting system tailored to my needs.

Svelte is a lightweight framework that allows you to write efficient and readable code. It's perfect for building small-scale applications like a custom comment system.

Astro is a modern web framework that enables me to build static site applications with ease. This allows me to create a seamless user experience, where comments are made by users with accounts through my site and stored in a secure database.

# The Code

```svelte
// Comment.svelte
<script lang="ts">
    export let comment: {id: string, member_id: string, name: string, avatar_image: string, replies?: {status: 'published' | 'hidden' | 'deleted', id: string, created_at: number, edited_at: number, html: string}[], email: string, parent_id: string | null, status: 'published' | 'hidden' | 'deleted', html: string, edited_at: number, created_at: number, update_at: number, like_count: number, liked_by_user: 0 | 1}
	import { format } from 'timeago.js'
	import { uuid, reply, editing, comments } from './signin'
	import { generateHSL, splitTextWithUrls } from '../lib/commentUtils'
	import Editor from './Editor.svelte';
	let actionMenu: HTMLDialogElement | undefined
	async function likeComment(comment: {id: string, member_id: string, name: string, avatar_image: string, replies?: {status: 'published' | 'hidden' | 'deleted', id: string, created_at: number, edited_at: number, html: string}[], email: string, parent_id: string | null, status: 'published' | 'hidden' | 'deleted', html: string, edited_at: number, created_at: number, update_at: number, like_count: number, liked_by_user: 0 | 1}) {
		const res = await fetch(`/members/api/comments/${comment.id}/like`, {
			method: comment.liked_by_user ? 'DELETE' : 'POST',
			headers: { 'Content-Type': 'application/json' }
		})
		if (!res.ok)
			return comment
		return { ...comment, liked_by_user: +(!comment.liked_by_user) as 0 | 1, like_count: comment.like_count + (+!comment.liked_by_user || -1) }
	}
	async function deleteComment(comment: {id: string, member_id: string, name: string, avatar_image: string, replies?: {status: 'published' | 'hidden' | 'deleted', id: string, created_at: number, edited_at: number, html: string}[], email: string, parent_id: string | null, status: 'published' | 'hidden' | 'deleted', html: string, edited_at: number, created_at: number, update_at: number, like_count: number, liked_by_user: 0 | 1}) {
		const res = await fetch(`/members/api/comments/${comment.id}`, {
			method: 'PUT',
			headers: { 'Content-Type': 'application/json' },
			body: JSON.stringify({ status: 'deleted', comment: comment.html })
		})
		if (!res.ok)
			return comment
		const { status, html } = await res.json()
		const updatedComment = { ...comment, status, html, replies: [] }
		$comments = $comments.map(c => {
			if (c.id === updatedComment.id)
				return updatedComment
			if (c.replies)
				return {...c, replies: c.replies.map(r => r.id === updatedComment.id ? updatedComment : r)}
			return c
		})
		return updatedComment
	}
</script>
{#if comment.status === 'published'}
	{@const { name, member_id, avatar_image, parent_id, id, created_at, replies, liked_by_user, like_count, html, edited_at } = comment}
	<div class="flex w-full flex-row mb-4">
		<div class="mr-3 flex flex-col items-center justify-start">
			<div class="flex-0 mb-4">
				<figure class="relative size-10">
					<div style:background-color={generateHSL(name)} class="flex items-center justify-center rounded-full size-full">
						<p class="text-lg text-light font-semibold">{name.split(' ').map((names) => names.charAt(0).toUpperCase()).join('')}</p>
					</div>
					<img srcset="{avatar_image}40 1x, {avatar_image}60 1.5x, {avatar_image}80 2x, {avatar_image}100 2.5x, {avatar_image}120 3x" src="{avatar_image}40" class="absolute left-0 top-0 rounded-full size-full text-transparent" alt="Profile" />
				</figure>
			</div>
			{#if parent_id === null}
				<div class="mb-2 h-full w-[3px] grow rounded bg-gradient-to-b from-black/5 via-black/5 to-transparent dark:from-white/5 dark:via-white/5" />
			{/if}
		</div>
		<div class="grow">
			<h4 class="font-semibold align-top">{name}</h4>
			<time datetime="{created_at.toString()}" title="{new Date(created_at).toLocaleString()}" class="opacity-50 text-sm">{format(created_at)}</time>
			{#if edited_at}
				<span title="{new Date(edited_at).toLocaleString()}" class="opacity-50 text-sm">· Edited</span>
			{/if}
			<p class="mt-1 prose dark:prose-invert break-words whitespace-pre-line">
				{#each splitTextWithUrls(html) as { type, content }}
					{#if type === 'text'}
						{content}
					{:else if type === 'url'}
						<a class="selection:[text-shadow:none] [text-shadow:-.7px_-.7px_0_#fdfdfd,.7px_-.7px_0_#fdfdfd,-.7px_.7px_0_#fdfdfd,.7px_.7px_0_#fdfdfd] dark:[text-shadow:-.7px_-.7px_0_#1d1f28,.7px_-.7px_0_#1d1f28,-.7px_.7px_0_#1d1f28,.7px_.7px_0_#1d1f28] no-underline bg-gradient-to-r from-average to-average bg-[left_95%] bg-no-repeat transition-[background-size] ease-[ease] duration-200 bg-[length:100%_.125rem] hover:bg-[length:0_.125rem] hover:bg-[right_95%]" href={content} rel="noopener noreferrer" target="_blank">{content}</a>
					{:else}
						<a class="selection:[text-shadow:none] [text-shadow:-.7px_-.7px_0_#fdfdfd,.7px_-.7px_0_#fdfdfd,-.7px_.7px_0_#fdfdfd,.7px_.7px_0_#fdfdfd] dark:[text-shadow:-.7px_-.7px_0_#1d1f28,.7px_-.7px_0_#1d1f28,-.7px_.7px_0_#1d1f28,.7px_.7px_0_#1d1f28] no-underline bg-gradient-to-r from-average to-average bg-[left_95%] bg-no-repeat transition-[background-size] ease-[ease] duration-200 bg-[length:100%_.125rem] hover:bg-[length:0_.125rem] hover:bg-[right_95%]" href="mailto:{content}">{content}</a>
					{/if}
				{/each}
			</p>
			<div class="flex items-center gap-5 mt-.5">
				<button disabled="{!$uuid}" on:click={async() => comment = await likeComment(comment)} data-liked="{!!liked_by_user}" class="group flex items-center text-sm transition-[color,opacity] ease-linear data-[liked=true]:text-dark dark:data-[liked=true]:text-light text-black/50 dark:text-white/50 enabled:hover:text-black/25 enabled:hover:dark:text-white/25" type="button">
					<svg xmlns="http://www.w3.org/2000/svg" class="motion-safe:group-data-[liked=true]:animate-[heartbeat_.75s_ease-in-out_forwards_infinite] size-4 mr-1.5 group-data-[liked=false]:stroke-current fill-transparent group-data-[liked=true]:fill-current" viewBox="0 0 16 16">
						<path d="M8 13.874 2.46 8.096a3.277 3.277 0 0 1-.613-3.785 3.278 3.278 0 0 1 5.248-.851L8 4.364l.905-.904a3.278 3.278 0 0 1 4.635 4.635L8 13.874Z"/>
					</svg>
					{like_count}
				</button>
				{#if parent_id === null && $uuid}
					<button on:click={() => $reply = $reply === id ? '' : id} data-replying="{$reply === id}" class="group flex items-center text-sm transition-[color,opacity] ease-linear data-[replying=true]:text-dark dark:data-[replying=true]:text-light text-black/50 hover:text-black/25 dark:text-white/50 dark:hover:text-white/25" type="button">
						<svg xmlns="http://www.w3.org/2000/svg" class="size-4 mr-1.5 fill-transparent group-data-[replying=true]:fill-current group-data-[replying=false]:stroke-current" viewBox="0 0 16 16">
							<path d="M7.175 2.987a.989.989 0 0 0-1.714-.67l-3.6 3.897a.989.989 0 0 0 0 1.34l3.598 3.898a.988.988 0 0 0 1.714-.67v-1.92h1.954a4.943 4.943 0 0 1 4.943 4.943V9.85a4.943 4.943 0 0 0-4.943-4.943H7.173l.002-1.92Z"/>
						</svg>
						Reply
					</button>
				{/if}
				{#if $uuid}
					<div class="relative">
						<button on:click={() => actionMenu?.show()} class="group flex items-center text-sm transition-[color,opacity] ease-linear" type="button">
							<svg xmlns="http://www.w3.org/2000/svg" class="size-4 fill-black/50 outline-none transition ease-linear hover:fill-black/75 dark:fill-white/50 dark:hover:fill-white/25">
								<path d="M3 12a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3Zm5 0a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3Zm5 0a1.5 1.5 0 1 0 0-3 1.5 1.5 0 0 0 0 3Z"/>
							</svg>
						</button>
						<dialog bind:this={actionMenu} class="z-10 mt-2 min-w-min sm:min-w-36 whitespace-nowrap rounded bg-neutral-900 dark:bg-white text-sm shadow-lg outline-none">
							<div class="flex flex-col text-sm dark:text-dark text-light z-10 shadow-dark dark:shadow-light">
								{#if $uuid === member_id}
									<button data-action="edit" on:click={() => $editing = id} class="pb-1.5 w-full text-left transition-colors hover:bg-neutral-700 dark:hover:bg-neutral-200 px-4 pt-3 rounded-t" type="button">Edit</button>
									<button data-action="delete" on:click={async() => comment = await deleteComment(comment)} class="pt-1.5 pb-3 w-full text-left transition-colors hover:bg-neutral-700 dark:hover:bg-neutral-200 rounded-b text-red-500 px-4" type="button">Delete</button>
								{/if}
							</div>
							<form class="fixed inset-0 text-transparent -z-10" method="dialog">
								<button class="h-full w-full cursor-default" type="submit">Close</button>
							</form>
						</dialog>
					</div>
				{/if}
			</div>
			{#if replies}
				<div class="mt-8">
					{#each replies.sort((a, b) => +new Date(a.created_at) - +new Date(b.created_at)) as reply}
						<svelte:self comment={reply} />
					{/each}
				</div>
			{/if}
		</div>
	</div>
{/if}
```

# The Benefits

With a custom commenting system built with Svelte and Astro, I've gained complete control over the look and feel of my comments section. I can customize the layout, add or remove features as needed, and even integrate other APIs to enhance the user experience.

Additionally, having a custom comment system allows me to:
- Better handle spam and moderation

- Improve performance and scalability

- Enhance the overall user experience

# Conclusion

Replacing Utterances with my own implementation built with Svelte and Astro has been a game-changer for my blog. With complete control over the commenting system, I can create a unique and engaging experience for my readers.

In addition, there are several new features I hope to add, such as notifications, reporting, and more. I'm excited to continue building and improving my custom comment system to make it even better.

If you're looking to customize your comments section or build a custom commenting system from scratch, I hope this post has inspired you to take the leap. If you'd like any more information on it, the code is available on [my GitLab repository](https://gitlab.com/1337_Nerd/blog). Happy coding!