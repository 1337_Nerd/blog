---
title: 'Refactoring the Backend of the HRDA Project'
pubDate: '2024-04-21T01:15:18.558Z'
description: 'My experiences refactoring the HRDA Project to use D1 databases as opposed to parsing a large JSON file'
author: 'Josh Stock'
featuredImage: '/src/assets/hero/hrda.png'
tags: ['work', 'hrda']
---

So I've been working on the [HRDA project](https://www.hrdaproject.org/) in collaboration with Dr. Kamatali. Part of that involves the backend, which up until recently has been largely whatever we could get running. Originally, the site was designed to use [Cloudflare Workers](https://developers.cloudflare.com/workers/), but we ran into a limitation I had never heard of where there is a limit on the [amount of connections open at once](https://developers.cloudflare.com/workers/platform/limits/#simultaneous-open-connections). Normally, this wouldn't cause any issues but because the page is loading a 15MB JSON file, if any more than roughly 20 people tried to hit a page at once, it would cause the workers to time out [in combination with the CPU time limit](https://developers.cloudflare.com/workers/platform/limits/#worker-limits).

```json
{
    "outcome": "exceededCpu",
    "scriptName": "pages-worker--1268848-production",
    "diagnosticsChannelEvents": [],
    "exceptions": [
        {
            "name": "Error",
            "message": "Worker exceeded CPU time limit.",
            "timestamp": 1693427732718
        }
    ],
    "logs": [],
    "eventTimestamp": 1693427732144,
    ...
}
```

The solution we settled on was moving to a different hosting provider with a more generous CPU timeout as well as setting up very aggressive caching (roughly 4 hours on the side of the CDN). However, as the database started to grow, the time to fetch the JSON file, parse it, build the page, and return it has started to grow. Currently, we're at roughly 6 seconds to load the worst-case scenario, which is every result across the entire database.

![All results loading time](/src/assets/images/pageLoad.jpg)

This is starting to get a bit out of hand. The main issue is that the worker on the backend has to fetch the JSON file, and then parse it as opposed to being able to read the entire file. Having done some debugging using the [server timing header](https://developer.mozilla.org/en-US/docs/Web/API/Performance_API/Server_timing), I found that the timings for building the page are essentially negligible, but that fetching the JSON file takes up the majority of the time to make this.

So because of this, there are a few possible ways to solve this. With the bottleneck being the fetch command, the easiest way to solve this is just by importing the JSON file instead of fetching it. While this does mostly solve the issue, it also massively increases the size of the backend js file, which I can do on neither Cloudflare nor Vercel.

The second solution is what I eventually went with, which some people are probably yelling at the screen: using an actual database. Part of my learning experience for [setting up this blog](/jwt-ghost) was learning about how the Cloudflare D1 database works and incorporating it into my site. Because of this (and the fact that it's now out of beta), I decided to try it out instead of a JSON file and...

![New page load time](/src/assets/images/newPageLoad.jpg)

That's so much better! Keep in mind, this is operating under the assumption that you're doing the most intensive task that you possibly could, which is essentially doing a `SELECT * FROM feedbacks_tbl LEFT JOIN country_tbl`. This involves reading 10,802 rows, which is still significantly faster than reading the entire JSON file. Keep in mind, because of some issues with node.js compatibility in Cloudflare workers, I can't get it any more precise than what it's reading out currently. With that being said, changing nothing other than the way the data is stored and reducing the loading time by 5 and a half seconds is absurd. Keep in mind, in this case, it still has to generate the page on the server before sending it over, which involves building a page with roughly 50,961 HTMLElementNodes. That being said, I'm sure some amount of the overhead is how it builds the page, as that hasn't been remotely optimized. Also, the timing code isn't that great. You'll notice I only have 4 things that it's measuring and really, it looks like I'm only measuring 2 things. That's not entirely wrong. Here's all the code on the backend for querying the database:

```ts
const start = Date.now()
const filteredData = await platform?.env.database.prepare(sql).bind(...params).all()
timings.push(`db;dur=${Date.now() - start};desc="Database query"`)
```

And that's it. I didn't really need more specific data from the backend, but here's the important part:

```ts
import type { Load } from '@sveltejs/kit'
export const load: Load = async({url, fetch, setHeaders}) => {
	const start = Date.now()
	const filtered = await fetch(`${env.databaseRoute}?${url.searchParams.toString()}`)
	const headers = filtered.headers.get('Server-timing')
	setHeaders({
		'server-timing': `${headers}, get;dur=${Date.now() - start};desc="Database query time"`
	})
	return await filtered.json()
}
```

This is the entirety of the `+page.server.ts` file for the results page. So essentially, what that means is that the database query on the database route is also the entirety of the loading time. Obviously, there's some logic to parse it out and put it into a more readable format I can manipulate on the results page, but that's not enough time to even be measurable. So what's the bottleneck at this point? As best as I can tell, constructing the page on the server.  If you inspect the results page, you'll see that I'm building the entire page by cycle, then issue, then country, and finally by value. This means I essentially have several nested loops, unfortunately, which can take a decent amount of time. However, this is still a great improvement! But, I need some amount of testing to ensure I can move back to Cloudflare for this. It's a bad test, but it gets the point across and that is:

```python
import requests
from concurrent.futures import ThreadPoolExecutor

workers = 100

urls = ['https://hrdaproject.org/results']
urls *= workers

def get_status(url):
    r = requests.get(url)
    return r.status_code
with ThreadPoolExecutor(max_workers=workers) as pool:
    print(list(pool.map(get_status, urls)))
```

Now, why is this a good test? Because I'm operating under the worst-case scenario here. Testing this without any caching, it will hit the database and pull 10,802 rows, parse it, and send it to the results page, which then builds the page and sends it to the user. And it does this 100 times all at once. Now keep in mind, because of how the backend works at Cloudflare, I have a limit for the amount of time that can be spent building this. This was actually how I did a stress test for the original site once we discovered that workers would time out and we had to move to Vercel. In fact, here are the results from when we switched from Cloudflare to Vercel, even with just parsing the very large JSON file:

![Stress test results](/src/assets/images/stressResults.png)

In the first one, you can see that there are quite a few 503s mixed in there along with the 200s. Every one of those is a connection timing out, giving the dreaded error 1102.

![Error 1102](/src/assets/images/error1102.png)

However, the second shows almost entirely 200s, with a few 504s, which are all just gateway timeouts. For the time being, ignore those. Now, what does it look like if I try this with the new design, using a D1 database?

![Updated stress test results](/src/assets/images/newStress.png)

That's incredible! Yeah, it still has a 503 error but considering the fact that there's only one and it's less than half the time without even caching the page means that there are almost no downsides to this. That being said, this comes with its own set of limitations. The biggest one is that you are limited to reading 5 million rows every day. Ordinarily, I would say this isn't an issue, but with the largest query taking up almost 11 thousand rows, you could probably get there pretty quickly. If we assume that you hit it with the worst-case scenario, that's 462 possible queries (assuming that you read almost 11,000 rows every time). However, implement some caching that lasts for 4 hours and that quadruples the amount. Obviously, that's the worst-case scenario and with caching, that makes it significantly more difficult to burn through all of this. That being said, I'm very happy with the change.

This entire post ended up being mostly me figuring out exactly why databases are used everywhere. While I haven't had the most experience with SQL outside of some internships, I am very appreciative of Cloudflare's very generous free plan as switching how the data is stored single-handedly saved several seconds on each page visit. And again, that's just for the worst-case scenario. If we assume a much more realistic scenario, like searching for a single country, that goes down to almost nothing. Here's an example, where I narrow the search down to just Bahrain.

![Bahrain results](/src/assets/images/bahrainSearch.jpg)

Once again, that's incredible! 134 ms to generate the entire page when it normally takes 3 seconds just to grab the data when using a JSON file. Look forward to this change going live sometime in the next week. Hopefully, this improves the general usefulness of the site.

As a side note, implementing this in the options filter is also a really simple way to see the improvement. Essentially, when you select a filter on the main page, you can see a small delay as it queries the database to see what elements are also in there. Changing absolutely nothing about how it parses or generates the data, which is horribly inefficient, it goes from taking roughly half a second when you select a country to roughly a quarter of a second. Again, you can see how most of this is limited by the response from the database, which is still incredibly quick all things considered.

Anyway, that's my most recent project and how that's been going. Unfortunately, this one isn't open source so I can't share the entirety of the code but this gives you a general idea of how the backend works. Look forward to a front-end redesign coming up in the near future as well!