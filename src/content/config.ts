import { defineCollection, z } from "astro:content";

const postsCollection = defineCollection({
	type: 'content',
	schema: () =>
		z.object({
			title: z.string(),
			pubDate: z.string(),
			description: z.string(),
			author: z.string(),
			featuredImage: z.string().optional(),
			tags: z.string().array().optional()
		})
})

export const collections = {
	posts: postsCollection
}