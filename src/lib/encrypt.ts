const encrypt = async (data: string, ENCRYPTION_SECRET: string, IV: string) => {
	const algorithm = {
		name: 'AES-GCM',
		iv: Uint8Array.from(IV.split(',').map(Number))
	}
	const encodedData = new TextEncoder().encode(data)
	const key = await crypto.subtle.importKey(
		'jwk',
		JSON.parse(ENCRYPTION_SECRET),
		{ name: 'AES-GCM', length: 256 },
		true,
		['encrypt', 'decrypt']
	)
	const encryptedData = await crypto.subtle.encrypt(
		algorithm,
		key,
		encodedData
	)
	return btoa(String.fromCharCode(...new Uint8Array(encryptedData)))
}

export default encrypt