const Sign = async (payload: Record<string, any>, secret: string, alg: string, name: string, kid: string) => {
	const header = {
		alg,
		kid,
		typ: 'JWT'
	}
	const encodedHeader = btoa(JSON.stringify(header)).replace(/=+$/, '')
	const encodedPayload = btoa(JSON.stringify(payload)).replace(/=+$/, '')
	const enc = new TextEncoder()
	const key = await crypto.subtle.importKey(
		'raw',
		enc.encode(secret),
		{ name, hash: 'SHA-384' },
		false,
		['sign', 'verify']
	)
	const signature = await crypto.subtle.sign(name, key, enc.encode(`${encodedHeader}.${encodedPayload}`))
	return `${encodedHeader}.${encodedPayload}.${btoa(String.fromCharCode(...new Uint8Array(signature))).replaceAll('/', '_').replaceAll('+', '-').replace(/=+$/, '')}`
}

export default Sign