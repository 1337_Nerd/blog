const getSHA384Hash = async (data: string) => {
	return Array.from(new Uint8Array(await crypto.subtle.digest('SHA-384', new TextEncoder().encode(data)))).map(b => b.toString(16).padStart(2, '0')).join('')
}

export default getSHA384Hash