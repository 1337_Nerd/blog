const encrypt = async (data: string, ENCRYPTION_SECRET: string, IV: string) => {
	const algorithm = {
		name: 'AES-GCM',
		iv: Uint8Array.from(IV.split(',').map(Number))
	}
	const key = await crypto.subtle.importKey(
		'jwk',
		JSON.parse(ENCRYPTION_SECRET),
		{ name: 'AES-GCM', length: 256 },
		true,
		['encrypt', 'decrypt']
	)
	const dehashedData = new Uint8Array(atob(data).split('').reduce((acc: number[], next: string) => [...acc, next.charCodeAt(0)], [])).buffer
	const decrypted = await crypto.subtle.decrypt(
		algorithm,
		key,
		dehashedData
	)
	return new TextDecoder().decode(decrypted)
}

export default encrypt
