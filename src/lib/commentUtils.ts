export function generateHSL(name: string) {
	const hash = Math.abs([...name].reduce((acc: number, char: string) => { return char.charCodeAt(0) + ((acc << 5) - acc) }, 0))
	const h = Math.floor((hash % (360 - 0)) + 0)
	const l = Math.floor((hash % (8 - 28)) + 28)
	return `hsl(${h}, 60%, ${l}%)`
}
export function splitTextWithUrls(text: string) {
	const urlRegex = /\b(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z0-9\u00a1-\uffff][a-z0-9\u00a1-\uffff_-]{0,62})?[a-z0-9\u00a1-\uffff]\.)+(?:[a-z\u00a1-\uffff]{2,}\.?))(?::\d{2,5})?(?:[/?#]\S*)?(?=\s|\b|$)/ig
	const emailRegex = /\b[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*(?=\s|\b|$)/ig
	const combinedRegex = new RegExp(`${urlRegex.source}|${emailRegex.source}`, 'ig')
	const parts = []
	let lastIndex = 0
	let match
	
	while ((match = combinedRegex.exec(text)) !== null) {
		if (match.index > lastIndex)
			parts.push({ type: 'text', content: text.slice(lastIndex, match.index) })
		if (match[0].match(emailRegex))
			parts.push({ type: 'email', content: match[0] })
		else if (match[0].match(urlRegex))
			parts.push({ type: 'url', content: match[0] })
		lastIndex = match.index + match[0].length
	}
	
	if (lastIndex < text.length)
		parts.push({ type: 'text', content: text.slice(lastIndex) })
	return parts
}