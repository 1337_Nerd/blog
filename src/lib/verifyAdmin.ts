import decrypt from "./decrypt";
import getSHA384Hash from "./hash";
import type { D1Database } from '@cloudflare/workers-types'

const Verify = async (JWT: string, secret: string, name: string) => {
	const enc = new TextEncoder()
	const key = await crypto.subtle.importKey(
		'raw',
		enc.encode(secret),
		{ name, hash: 'SHA-384' },
		false,
		['sign', 'verify']
	)
	const signature: BufferSource = new Uint8Array(atob(JWT.split('.')[2].replace(/_/g, '/').replace(/-/g, '+')).split('').reduce((acc: number[], next: string) => [...acc, next.charCodeAt(0)], [])).buffer
	return await crypto.subtle.verify(name, key, signature, enc.encode(`${JWT.split('.')[0]}.${JWT.split('.')[1]}`))
}

async function verifyAdmin(JWT: string | null | undefined, DB: D1Database, KEYS: D1Database, ENCRYPTION_SECRET: string, IV: string, url: string) {
	if (!JWT)
		return false
	const { iat, exp, aud, iss, sub } = JSON.parse(atob(JWT.split('.')[1].replace(/_/g, '/').replace(/-/g, '+')))
	if (iss !== url)
		return false
	const now = new Date().getTime() / 1000
	if (exp < now || iat > now)
		return false
	const { kid } = JSON.parse(atob(JWT.split('.')[0].replace(/_/g, '/').replace(/-/g, '+')))
	const key = await KEYS.prepare('SELECT secret, algorithmName FROM keys WHERE kid = ?').bind(kid).first<{secret: string, algorithmName: string}>()
	if (!key)
		return false
	const { secret, algorithmName } = key
	const decryptedSecret = await decrypt(secret, ENCRYPTION_SECRET, IV)
	const verified = await Verify(JWT, decryptedSecret, algorithmName)
	if (!verified)
		return false
	const hashedAuth = await getSHA384Hash(`${aud}${sub}`)
	const results = await DB.prepare('SELECT * FROM tokens WHERE access_token = ? AND user_id = ? AND iat < ? AND exp > ?').bind(hashedAuth, aud, now, now).first<{exp: number, iat: number, id: string, email: string, name: string}>()
	if (!results || results.exp < now || results.iat > now)
		return false
	return {
		id: results.id,
		email: results.email,
		name: results.name,
		iat: results.iat,
		exp: results.exp
	}
}

export default verifyAdmin