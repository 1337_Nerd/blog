import type { D1Database } from '@cloudflare/workers-types'
import getSHA384Hash from './hash'

export const timings: string[] = []

function res(message: string, status: number): Response {
	return new Response(message, {
		headers: {
			'Set-Cookie': 'sessionId=; path=/; expires=Thu, 01 Jan 1970 00:00:00 GMT; samesite=lax; secure; httponly',
			'Server-Timing': timings.join(',')
		},
		status
	})
}

const Verify = async(cookies: string | null, DB: D1Database): Promise<Response | { id: string, email: string, name: string, subscribed: 0 | 1, trackVisits: 0 | 1, iat: number, exp: number }> => {
	timings.length = 0
	const sessionId = cookies?.split('; ').find(cookie => cookie.startsWith('sessionId'))?.split('=')[1]
	if (!sessionId)
		return new Response(null, {status: 204})
	const hashedAuth = await getSHA384Hash(sessionId)
	const start = performance.now()
	const { results } = await DB.prepare('SELECT id, email, name, subscribed, trackVisits, iat, exp FROM users LEFT JOIN tokens ON users.id = tokens.user_id WHERE access_token = ?').bind(hashedAuth).all<{id: string, iat: number, exp: number, email: string, name: string, subscribed: 0 | 1, trackVisits: 0 | 1}>()
	timings.push(`userSearch;dur=${performance.now() - start};desc="search for user"`)
	if (results.length < 1)
		return res('User not found or invalid token', 404)
	const { id, iat, exp } = results[0]
	const now = new Date().getTime() / 1000
	if (exp < now || iat > now) {
		const tokenStart = performance.now()
		const { success } = await DB.prepare('DELETE FROM tokens WHERE access_token = ?').bind(hashedAuth).run()
		timings.push(`deleteToken;dur=${performance.now() - tokenStart};desc="delete invalid access token"`)
		if (!success)
			return res('Error deleting token', 500)
		return res('Token expired', 400)
	}
	const updateLast = performance.now()
	await DB.prepare('UPDATE users SET lastSeen = ? WHERE id = ? AND trackVisits = ?').bind(new Date().toISOString(), id, true).run()
	timings.push(`updateSeen;dur=${performance.now() - updateLast};desc="update last seen"`)
	return results[0]
}
export default Verify