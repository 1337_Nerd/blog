async function sendLink(email: string, userId: string, link: string, api: string, url: string, args: Record<string, string> = {}) {
	const template = new URL(`${url}members?r=${encodeURIComponent(link)}`)
	for (const [key, value] of Object.entries(args))
		template.searchParams.append(key, value)
	try {
		const res = await fetch('https://v4.passwordless.dev/magic-links/send', {
			headers: {
				'ApiSecret': api,
				'Content-Type': 'application/json'
			},
			method: 'POST',
			body: JSON.stringify({
				'emailAddress': email,
				'urlTemplate': `${template.toString()}&token=$TOKEN`,
				'userId': userId
			})
		})
		if (res.ok)
			return new Response('Success sending email', {status: 200})
		return new Response(JSON.stringify({'error': 'Error sending email'}), { status: 500, headers: { 'Content-Type': 'application/json' } })
	}
	catch (error) {
		return new Response(JSON.stringify({'error': 'Unspecified error sending email'}), { status: 500, headers: { 'Content-Type': 'application/json' } })
	}
}

export default sendLink